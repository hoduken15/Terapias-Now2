<?php

Route::get('/spa', 'HomeController@spa')->name('spa');

Route::group(['middleware' => 'auth'], function(){
	Route::resource('terapias', 'TerapiaController');
	Route::resource('perfil', 'PerfilController');
	Route::resource('schedule', 'ScheduleController');
	Route::resource('atenciones', 'AtencionesController');
	Route::resource('comentarios', 'ComentariosController');
	Route::resource('user', 'UserController');
	Route::resource('palabras', 'PalabrasController');
	Route::resource('PalabrasTerapias', 'PalabrasTerapiasController');
	Route::resource('noticias', 'NoticiasController');
	Route::resource('solicitudes', 'SolicitudesController');
});

Route::group(['middleware' => 'auth'], function(){
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/home/{id}/', 'HomeController@show')->name('home.noticias');
	Route::get('/solicitudes/aprobar/{id}', 'SolicitudesController@aprobado')->name('admin.aprobar');
	Route::get('/solicitudes/rechazar/{id}', 'SolicitudesController@rechazado')->name('admin.rechazar');
	Route::post('/palabras/eliminar', 'PalabrasController@elimina')->name('palabras.elimina');
	Route::get('/SugerenciaTerapias', 'SugerenciaTerapiasController@index')->name('sugerenciaTerapias');
	Route::get('/terapias/anadir/{user}', 'TerapiaController@solicitarTerapia')->name('terapias.añadir');
	Route::post('/terapias/solicitar/{user}', 'TerapiaController@solicitar')->name('terapias.solicitar');
	Route::post('/terapias/sugerir/{user}', 'TerapiaController@sugerir')->name('terapias.sugerir');
	Route::get('/atenciones/ver/{user}', 'AtencionesController@verAtenciones')->name('atenciones.ver');
	// Route::get('/user/terapia/{id}', 'TerapiaController@userupdate')->name('terapia.userupdate');
	Route::get('/user/{id}/formulario', 'UserController@formulario')->name('user.formulario');
	Route::post('/user/{id}/solicitud-terapeuta', 'UserController@solicita')->name('user.solicita');
	Route::get('/user/descripcion/{id}', 'UserController@descripcion')->name('user.descripcion');
	Route::get('/user/descripcion/update/{id}', 'UserController@descripcionupdate')->name('user.descripcionupdate');
	Route::get('/comentarios/user/{id}/', 'ComentariosController@showuser')->name('comentarios.showuser');
	Route::get('/comentarios/terapeuta/{id}/', 'ComentariosController@showterapeuta')->name('comentarios.showterapeuta');
	Route::get('/notificaciones', 'NotificacionesController@index')->name('notificaciones');
	Route::get('/notificaciones/{id}/dismiss', 'NotificacionesController@dismiss')->name('notificaciones.dismiss');
	Route::get('/comentarios/user/crea/{id}', 'ComentariosController@comentar')->name('comentarios.comentar');
	Route::get('/comentarios/terapeuta/crea/{id}', 'ComentariosController@comentaterapeuta')->name('comentarios.comentaterapeuta');
	Route::post('/comentarios/user/store/{id}', 'ComentariosController@guardar')->name('comentarios.guardar');
	Route::post('/comentarios/terapeuta/store/{id}', 'ComentariosController@guardarterapeuta')->name('comentarios.guardarterapeuta');
});

Route::group(['prefix'=> 'admin','middleware' => 'auth'], function(){
	Route::get('/noticias/listar', 'NoticiasController@listar')->name('noticias.listar');
	Route::get('/terapias/listar', 'TerapiaController@listar')->name('terapias.listar');
	Route::get('/terapias/{id}', 'TerapiaController@muestra')->name('terapias.muestra');
	Route::get('/terapeutas/{id}', 'UserController@verterapeuta')->name('users.verterapeuta');
	Route::get('/terapeutas/edit/{id}', 'UserController@adminedit')->name('users.adminedit');
	Route::get('/terapeutas/update/{id}', 'UserController@adminupdate')->name('users.adminupdate');
	Route::get('/terapeutas/destroy/{id}', 'UserController@admindestroy')->name('users.admindestroy');
	Route::get('/terapeutas', 'AdminController@terapeutas')->name('admin.terapeutas');
	Route::get('/usuarios', 'AdminController@usuarios')->name('admin.usuarios');
	Route::get('/terapias', 'AdminController@terapias')->name('admin.terapias');
	Route::get('/user/{id}', 'UserController@show')->name('admin.user.show');
	Route::get('/palabras/terapias', 'AdminController@showPalabrasTerapias')->name('sintomas.index');
	Route::post('/palabras/terapias/add', 'AdminController@PalabrasTerapias')->name('admin.palabras');
	Route::get('/palabras/terapias/add', 'AdminController@PalabrasTerapias')->name('admin.palabras');
	Route::get('/terapia/asignar/{id}', 'AdminController@verAgregaTerapia')->name('admin.verasigna');
	Route::post('/terapia/asigna/{id}', 'AdminController@AgregaTerapia')->name('admin.asigna');
});

Route::group(['prefix'=> 'perfil','middleware' => 'auth'], function(){
	Route::get('/{id}/comentarios', 'ComentariosController@show')->name('perfil.comentarios');
	Route::get('/{id}/terapias', 'TerapiaController@show')->name('perfil.terapias');
	Route::get('/{id}/horario', 'ScheduleController@show')->name('perfil.horario');
	Route::get('/{id}/agendamiento/{terapia}', 'AtencionesController@index')->name('perfil.hora');
	Route::get('/{id}/{terapia}', 'TerapiaController@ver')->name('perfil.miterapia');
});

Route::group(['prefix'=> 'search','middleware' => 'auth'], function(){
	Route::post('/','SearchController@index')->name('search');
	Route::get('/','SearchController@index')->name('search');
	Route::get('/terapia/{id}/show','SearchController@showterapia')->name('search.showterapia');
	Route::get('/terapeuta/{id}/show','PerfilController@show')->name('search.showterapeuta');
	Route::post('/sugerencia','SearchController@sugerencia')->name('search.sugerencia');
});

Auth::routes();

