<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Usuario no encontrado, favor verifique el Email o contraseña!',
    'throttle' => 'Demasiados intentos de ingreso. Por favor, intentelo denuevo en :seconds segundos.',

];
