<div class="fixed bg-secondary" style="width: all;">
    <div id="logo" class="p-2 navbar navbar-light bg-primary shadow-sm justify-content-between" style="padding-right: unset; padding-left: unset;">
        <a id="openNav" href="javascript:void(0)" class="navbar-brand" style="width: 50px; height: 30px; outline: none;" onclick="toggleNav()"><i class="fas fa-bars" style="z-index: 3;"></i></a>
        <input type="text" id="estadoNav" value="close" style="display: none">
        <a class="navbar-brand" style="color: #EEE7F3; font-size: 20px;" href="{{ route('home') }}">
            {{config('app.name')}}
        </a>
        <a class="navbar-brand" href="{{ route('perfil.index') }}">
            @if(isset(auth()->user()->imagen))
                <img src="/imagenes/usuarios/{{auth()->user()->imagen}}" alt="Usuario-{{$user->id}}" style="width: 40px; height: 40px;" class="rounded-circle"/>
            @else
                <img src="/imagenes/usuarios/default-user.jpg" alt="Usuario-{{$user->id}}" style="width: 40px; height: 40px;" class="rounded-circle"/>
            @endif
        </a>
    </div>
    <div id="search">
        <form id="buscar" method="POST" class="col-12" action="{{ route('search')}}" > @csrf
            <div class="container-fluid" style="margin-bottom: 10px;">
                <div class="navbar">
                    <div class="input-group">
                        <input type="text" 
                            name="buscar" 
                            class="form-control rounded-pill bg-fondo" 
                            style="border: none;" 
                            placeholder="Buscar..." 
                            aria-label="Buscar..." 
                            aria-describedby="basic-addon2"
                        >
                    </div>
                    <a href="#" onclick="document.forms['buscar'].submit();">
                        <i class="fas fa-search" style="position: fixed; top: 80px; right: 20px; font-size: 20px;"></i>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="mySidenav" class="sidenav bg-secondary fixed-left">
    @auth
        <a class="nav-link" href="{{ route('perfil.index')}}">Perfil</a>
        <a class="nav-link" href="{{ route('home') }}">Noticias</a>
        <a class="nav-link" href="{{ route('notificaciones')}}">Notificaciones</a>
        @if(auth()->user()->id_perfil == '1')
            <a class="nav-link" href="{{ route('atenciones.show', auth()->user()) }}">Mis Horas</a>
            <a class="nav-link" href="{{ route('comentarios.showuser', auth()->user()) }}">Mis Calificaciones</a>
        @elseif(auth()->user()->id_perfil == '2')
            <a class="nav-link" href="{{ route('terapias.show', auth()->user()) }}">Mis Terapias</a>
            <a class="nav-link" href="{{ route('schedule.index')}}">Mi Horario</a> {{-- Corregir, deberia ir directamente a su horario --}}
            <a class="nav-link" href="{{ route('perfil.comentarios', auth()->user()) }}">Mis Calificaciones</a>
            <a class="nav-link" href="{{ route('atenciones.ver', auth()->user()) }}">Mis Atenciones</a>
            <a class="nav-link" href="{{ route('terapias.añadir', auth()->user()) }}">Solicitar Terapias</a>
            <a class="nav-link" href="{{ route('atenciones.show', auth()->user()) }}">Mis Horas</a>
        @elseif(auth()->user()->id_perfil == '3')
            <a class="nav-link" href="{{ route('terapias.create')}}">Añadir Terapias</a>
            <a class="nav-link" href="{{ route('palabras.index')}}">Añadir Palabras Clave</a>
            <a class="nav-link" href="{{ route('sintomas.index') }}">Asignar Palabras a Terapias</a>
            <a class="nav-link" href="{{ route('atenciones.show', auth()->user()) }}">Mis Horas</a>
        @endif
        <a class="nav-link" href="{{ route('sugerenciaTerapias') }}">Sugerencia de Terapias</a>
        <a class="nav-link" href="{{ route('user.edit', auth()->user()) }}">Actualizar mi perfil</a>
        <hr>
        <a class="nav-link" href="#" onclick="document.getElementById('logout-form').submit();" >Cerrar Sesión</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
    @else
        <a class="nav-link" href="{{route('login')}}">Iniciar Sesión</a>
    @endauth
</div>
<script>
    function toggleNav() {
        var x = document.getElementById("estadoNav").value;
        if(x == "close"){
            document.getElementById("mySidenav").style.width = "200px";
            document.getElementById("estadoNav").value = "open";
        } else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("estadoNav").value = "close";
        }
    }
</script>