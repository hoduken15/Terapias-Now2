<nav class="navbar navbar-light bg-primary shadow-sm justify-content-between">
    <div class="container col-12" style="padding-right: unset; padding-left: unset;">
        <button class="navbar-toggler" type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="{{ __('Toggle navigation')}}"
        ><span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{ route('home') }}">
            {{config('app.name')}}
        </a>
        <a class="navbar-brand" href="{{ route('perfil.index') }}">
            <img src="/imagenes/usuarios/{{$user->imagen}}" alt="Usuario-{{$user->id}}" class="rounded-circle"/>
        </a>
        <div class="row collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav nav-pills">
                @auth
                    <li class="nav-item bg-secondary rounded" style="margin-left: 2px;">
                        <a class="nav-link {{-- {{setActive('perfil')}} --}}" 
                            href="{{ route('perfil.index')}}"
                        >Perfil</a>
                    </li>
                    <li class="nav-item bg-secondary rounded" style="margin-left: 2px;">
                        <a class="nav-link {{-- {{setActive('notificaciones')}} --}}"
                            href="{{ route('notificaciones')}}"
                        >Notificaciones</a>
                    </li>
                    @if(auth()->user()->id_perfil == '2')
                        <li class="nav-item bg-secondary rounded" style="margin-left: 2px;">
                            <a class="nav-link {{-- {{setActive('notificaciones')}} --}}"
                                href="{{ route('terapias.show', $user) }}"
                            >Mis Terapias</a>
                        </li>
                        <li class="nav-item bg-secondary rounded" style="margin-left: 2px;">
                            <a class="nav-link {{-- {{setActive('notificaciones')}} --}}"
                                href="{{ route('schedule.index')}}"
                            >Mi Horario</a>
                        </li>
                    @endif
                    @if(auth()->user()->id_perfil == '3')
                        <li class="nav-item bg-secondary rounded" style="margin-left: 2px;">
                        <a class="nav-link {{-- {{setActive('notificaciones')}} --}}"
                            href="{{-- {{ route('terapias')}} --}}"
                        >Añadir Terapias</a>
                        </li>
                    @endif
                    <li class="nav-item bg-secondary rounded" style="margin-left: 2px;">
                        <a class="nav-link" 
                            href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"
                        >Cerrar Sesión</a>
                    </li>
                @else
                    <li class="nav-item bg-secondary rounded" style="margin-left: 2px;">
                    <a class="nav-link {{-- {{setActive('login')}} --}}" href="{{route('login')}}">Iniciar Sesión</a>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
</nav>
<div class="bg-secondary" style="width: all;">
    <form method="POST" class="col-12" action="{{ route('search')}}" > @csrf
        <div class="container-fluid" style="margin-bottom: 10px;">
            <div class="navbar">
                <input type="text" 
                    name="buscar" 
                    class="form-control rounded bg-fondo" 
                    style="border: none;" 
                    placeholder="Buscar..." 
                    aria-label="Buscar..." 
                    aria-describedby="basic-addon2"
                >
            </div>
        </div>
    </form>
</div>