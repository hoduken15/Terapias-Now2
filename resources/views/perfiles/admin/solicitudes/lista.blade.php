@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	@if(isset($solicitudes) && !$solicitudes->isEmpty())
		@foreach($solicitudes as $solicitud)
			<div class="row rounded" style="margin-left: 10px; margin-right: 10px; margin-bottom: 10px;">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xl-12 border bg-secondary">
					<div class="row">
						<div class="col-4 col-lg-3 col-md-4 col-sm-4 col-xl-6 align-middle">
							<img src="/imagenes/usuarios/{{$solicitud->Usuarios->imagen}}" alt="Usuario-{{$solicitud->Usuarios->id}}" class="img-fluid rounded" style="height: 80px; width: 80px;" />
						</div>
						<div class="col-4 col-lg-5 col-md-5 col-sm-5 col-xl-6 ">
							<a href="#" data-toggle="modal" data-target="#modal-{{ $solicitud->id }}" style="color: black;">
								<h5 class="text-primary">Solicitante: {{ $solicitud->Usuarios->name }}</h5>
								<div>
									Terapia: {{ $solicitud->Terapias->nombre }}
								</div>
								<div>
									Valor: {{ $solicitud->precio }}
								</div>
							</a>
						</div>
						<div class="col-4 col-lg-2 col-md-3 col-sm-3 col-xs-12 ">
							@if(isset($solicitud->created_at))
								<div class="col-lg-2 col-md-2 col-sm-2 col-xl-12 ">
									<small>Fecha: {{ $solicitud->created_at->format('d-m-Y') }}</small>
								</div>
							@endif
							<div class="col-lg-2 col-md-2 col-sm-2 col-xl-12 ">
								<small>Tipo: {{ $solicitud->tipo_solicitud }}</small>
							</div>
						</div>
						<div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xl-12 justify-content-between">
							<a href="{{ route('admin.aprobar', $solicitud) }}" class="btn btn-lg btn-block btn-success ">
								<i class="far fa-check-circle"></i> Aprobar
							</a>
							<a href="{{ route('admin.rechazar', $solicitud) }}" class="btn btn-lg btn-block btn-danger">
								<i class="fas fa-ban"></i> Rechazar
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal-{{ $solicitud->id }}">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<a href="#" type="button" style="padding: 1rem 1rem;" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</a>
							<h2 class="modal-title col">{{ $solicitud->tipo_solicitud }}</h2>
						</div>
						<div class="modal-body">
							<p>{{ $solicitud->descripcion }}</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No existen solicitudes.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection