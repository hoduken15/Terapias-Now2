@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif
<div class="container">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Mantenedor de Usuarios</h3>
		</div>
		<a href="{{route('user.index')}}" type="button" class="btn btn-secondary btn-lg btn-block">Listar Usuarios</a>
	</div>
</div>
@endsection