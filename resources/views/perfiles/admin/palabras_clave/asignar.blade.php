@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Asignar Palabras a una Terapia</h3>
		</div>
		<form role="form" method="POST" action="{{ route('admin.palabras') }}">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="terapia">Terapias</label>
					<select
						name="terapia"
						class="form-control"
						id="terapia" 
					>
						<option value="0">Seleccione una Terapia</option>
						@foreach($terapias as $terapia)
						    <option value="{{$terapia->id}}"> {{ $terapia->nombre }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="palabra">Palabras</label>
					<select
						name="palabra"
						class="form-control"
						id="palabra" 
					>
						<option value="0">Seleccione una Palabra</option>
						@foreach($palabras as $palabra)
						    <option value="{{$palabra->id}}"> {{ $palabra->palabra }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-success">Asignar Palabras</button>
				<a class="btn btn-secondary" href="javascript:history.back()">Volver</a>
			</div>
		</form>
	</div>
</div>

@endsection