@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="col-lg-6 col-sm-12 col-md-12">
				<div class="col-12">
					<div class="box-header with-border">
						<h3 class="box-title">Agregar una Palabra</h3>
					</div>
					<form role="form" method="POST" action="{{ route('palabras.store') }}">
						{{ csrf_field() }}
						<div class="box-body">
							<div class="form-group">
								<label for="palabra">Palabra</label>
								<input 
									name="palabra" 
									type="text" 
									class="form-control" 
									id="palabra"
									placeholder="Ingresa la Palabra Clave"
									required
								>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-success">Añadir Palabra</button>
						</div>
					</form>
				</div>
				<hr>
				<div class="col-12">
					<div class="box-header with-border">
						<h3 class="box-title">Eliminar una Palabra</h3>
					</div>
					<form role="form" method="POST" action="{{ route('palabras.elimina') }}">
						{{ csrf_field() }} 
						<div class="box-body">
							<div class="form-group">
								<label for="palabra">Palabra</label>
								<select
									name="palabra"
									class="form-control"
									id="palabra" 
								>
									<option value="0">Seleccione una palabra</option>
									@foreach($palabras as $palabra)
									    <option value="{{$palabra->id}}"> {{ $palabra->palabra }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-danger">Eliminar Palabra</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-lg-6 col-sm-12 col-md-12" style="margin-bottom: 100px;">
				<h4>Palabras Actuales en Sistema</h4>
				<div class="box-footer">
					<ul id="theList" class="d-flex justify-content-start row" style="padding-left: 5px;list-style-type: none; margin-right: 0px;">
						@foreach ($palabras as $palabra)
							<li class="tagitem col-sm-6 col-6 col-md-4 col-lg-3 col-xl-3">{{ $palabra->palabra }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection