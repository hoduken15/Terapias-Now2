@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

<div class="container">
	@if(isset($usuarios))
		<div class="row">
			@foreach($usuarios as $usuario)
				<div class="col-lg-3 col-xs-6 p-2">
					<div class="small-box bg-secondary p-4">
						<div class="inner">
							<h3>{{ $usuario->name }}</h3>
							<small>{{ $usuario->email }}</small>
						</div>
						<div class="icon">
							<i class="fa fa-map-marker"></i> {{ $usuario->Centros->name }}
						</div>
						<a href="{{ route('admin.user.show',$usuario)}}" class="small-box-footer">
							Ver más <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
			@endforeach
		</div>
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No existen usuarios.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection