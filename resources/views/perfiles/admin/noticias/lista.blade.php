@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	@if(isset($noticias))
		@foreach($noticias as $noticia)
			<a href="{{ route('noticias.show', $noticia) }}">
				<div class="row border bg-secondary rounded" style="margin-left: 10px; margin-right: 10px; margin-bottom: 10px;">
					<div class="col-12">
						<div class="row">
							<div class="col-4 align-middle">
								<img src="/imagenes/noticias/{{$noticia->imagen}}" alt="Noticia-{{$noticia->id}}" class="img-fluid rounded"/>
							</div>
							<div class="col-5">
								<h5 class="text-primary">{{ $noticia->titulo }}</h5>
								<p>{{ $noticia->extracto }}</p>
							</div>
							<div class="col-3">
								@if(isset($noticia->created_at))
									<small>{{ $noticia->created_at->format('d-m-Y') }}</small>
								@endif
							</div>
						</div>
					</div>
				</div>
			</a>
		@endforeach
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No existen noticias.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection