@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="container">
		<div class="d-flex row justify-content-center">
			<div class="col-12 d-flex">
				<div class="card" style="width: 18rem;">
					<img src="/imagenes/noticias/{{$noticia->imagen}}" alt="Noticia-{{$noticia->id}}" class="card-img-top rounded img-fluid"/>
					<div class="card-body text-center">
						<h5 class="card-title">{{ $noticia->titulo }}</h5>
						<p class="card-text">{{ $noticia->detalle }}</p>
						<a href="{{ route('noticias.edit', $noticia) }}" type="button" class="btn btn-secondary btn-lg btn-block">Editar Noticia</a>
						<form method="POST" action="{{ route('noticias.destroy', $noticia) }}">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-secondary btn-lg btn-block">Eliminar Noticia</button>
						</form>
						<a href="{{ route('noticias.listar') }}" type="button" class="btn btn-secondary btn-lg btn-block">Volver</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection