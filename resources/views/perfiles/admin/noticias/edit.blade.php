@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="box-header with-border col-8 align-left">
				<h3 class="box-title">Actualizar Noticia</h3>
			</div>
		</div>
		<form role="form" method="POST" action="{{ route('noticias.update', $noticia) }}" enctype="multipart/form-data">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="titulo">Titulo</label>
					<input 
						name="titulo" 
						type="text" 
						class="form-control" 
						id="titulo" 
						placeholder="Ingresa el titulo" 
						value="{{ $noticia->titulo }}"
					>
				</div>
				<div class="form-group">
					<label for="extracto">Extracto</label>
					<input 
						name="extracto" 
						type="text" 
						class="form-control" 
						id="extracto" 
						placeholder="Ingresa el extracto" 
						value="{{ $noticia->extracto }}"
					>
				</div>
				<div class="form-group">
					<label for="detalle">Detalle</label>
					<input 
						name="detalle" 
						type="text" 
						class="form-control" 
						id="detalle" 
						placeholder="Ingresa el detalle" 
						value="{{ $noticia->detalle }}"
					>
				</div>
				<div class="form-group">
					<label for="imagen">Imagen de la Noticia</label>
					<input name="imagen" type="file" id="imagen">
				</div>
			</div>
			<div class="box-footer p-2">
				<button type="submit" class="btn btn-success">Actualizar</button>
			</div>
		</form>
	</div>
</div>

@endsection