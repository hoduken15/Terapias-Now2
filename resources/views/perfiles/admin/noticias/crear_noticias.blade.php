@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Crear una Noticia</h3>
		</div>
		<form role="form" method="POST" action="{{ route('noticias.store') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="titulo">Titulo de la Noticia</label>
					<input 
						name="titulo" 
						type="text" 
						class="form-control" 
						id="titulo" 
						placeholder="Ingresa el Titulo de la Noticia" 
						required
					>
				</div>
				<div class="form-group">
					<label for="detalle">Detalle de la Noticia</label>
					<textarea 
						name="detalle" 
						type="textarea"
						class="form-control" 
						id="detalle" 
						placeholder="Ingresa el Detalle de la Noticia" 
						required
					></textarea>
				</div>				
				<div class="form-group">
					<label for="fecha">Fecha de la Noticia</label>
					<input 
						name="fecha" 
						type="date" 
						class="form-control" 
						id="fecha" 
					>
				</div>
				<div class="form-group">
					<label for="imagen">Imagen de la Noticia</label>
					<input 
						name="imagen" 
						type="file" 
						class="form-control" 
						id="imagen" 
					>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-success">Crear Noticia</button>
				<a class="btn btn-secondary" href="javascript:history.back()">Volver</a>
			</div>
		</form>
	</div>
</div>

{{-- <script src="../../bower_components/ckeditor/ckeditor.js"></script> --}}

@endsection