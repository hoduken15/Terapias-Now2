@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	@if(isset($terapias))
		<div class="row">
			@foreach($terapias as $terapia)
				<div class="col-12 col-sm-12 col-md-6 col-lg-6">
					<a href="{{ route('terapias.muestra', $terapia) }}">
						<div class="row border bg-secondary rounded" style="margin-left: 10px; margin-right: 10px; margin-bottom: 10px;">
							<div class="col-12">
								<div class="row">
									<div class="col-4 align-middle">
										@if (isset($terapia->imagen))
											<img src="/imagenes/terapias/{{$terapia->imagen}}" alt="terapia-{{$terapia->id}}" class="img-fluid rounded" style="height: 120px; width: 120px;" />
										@else
											<img src="/imagenes/terapias/default.jpg" alt="terapia-{{$terapia->id}}" class="img-fluid rounded" style="height: 120px; width: 120px;" />
										@endif
									</div>
									<div class="col-5">
										<h5 class="text-primary">{{ $terapia->nombre }}</h5>
										{{-- <p>{{ $terapia->descripcion }}</p> --}}
									</div>
									<div class="col-3">
										@if(isset($terapia->created_at))
											<small>{{ $terapia->created_at->format('d-m-Y') }}</small>
										@endif
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			@endforeach
		</div>
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No existen terapias.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection