@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Actualizar una Terapia</h3>
		</div>
		<form role="form" method="POST" action="{{ route('terapias.update', $terapia) }}" enctype="multipart/form-data">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="nombre">Nombre de la Terapia</label>
					<input 
						name="nombre" 
						type="text" 
						class="form-control" 
						id="nombre" 
						value="{{ $terapia->nombre }}"
						disabled
					>
				</div>
				<div class="form-group">
					<label for="descripcion">Descripción de la Terapia</label>
					<input 
						name="descripcion" 
						type="text"
						class="form-control" 
						id="descripcion" 
						placeholder="Ingresa la Descripción de la Terapia"
						value="{{ $terapia->descripcion }}"
						required
					></input>
				</div>
				<div class="form-group">
					<label for="imagen">Imagen de la Terapia</label>
					<input 
						name="imagen" 
						type="file"
						class="form-control" 
						id="imagen" 
					></input>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-success">Actualizar Terapia</button>
				<a class="btn btn-secondary" href="javascript:history.back()">Volver</a>
			</div>
		</form>
	</div>
</div>

@endsection