@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="container">
		<div class="d-flex row">
			<div class="col-12 col-lg-6 col-md-6 col-sm-12">
				<div class="card" style="width: 18rem;">
					@if(isset($terapia->imagen))
						<img src="/imagenes/terapias/{{$terapia->imagen}}" alt="Terapia-{{$terapia->id}}" class="card-img-top rounded img-fluid"/>
					@else
						<img src="/imagenes/terapias/default.jpg" alt="Terapia-{{$terapia->id}}" class="card-img-top rounded img-fluid"/>
					@endif
					<div class="card-body text-center">
						<h5 class="card-title">{{ $terapia->nombre }}</h5>
						<p class="card-text">{{ $terapia->descripcion }}</p>
						<a href="{{ route('terapias.edit', $terapia) }}" type="button" class="btn btn-secondary btn-lg btn-block">Editar terapia</a>
						<form method="POST" action="{{ route('terapias.destroy', $terapia) }}">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-secondary btn-lg btn-block">Eliminar terapia</button>
						</form>
						<a href="{{ route('admin.verasigna', $terapia) }}" type="button" class="btn btn-secondary btn-lg btn-block">Añadir Terapeuta</a>
						<a href="{{ route('terapias.listar') }}" type="button" class="btn btn-secondary btn-lg btn-block">Volver</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-6 col-md-6 col-sm-12 text-center bg-secondary">
				<h1>Terapeutas que imparten esta terapia</h1>
				@if($terapia->Terapias->isNotEmpty())
					<table class="table table-bordered">
						<tr>
							<th>Terapeuta</th>
							<th>Correo</th>
						</tr>
						@foreach($terapia->Terapias as $terapeuta)
							<tr>
								<td>{{ $terapeuta->name }}</td>
								<td>{{ $terapeuta->email }}</td>
							</tr>
						@endforeach
					</table>
				@else
					<div class="text-center">
						<div class="col-lg-12 col-md col-xs-6 p-2 text-center">
							<div class="small-box p-4">
								<div class="inner">
									<h3>Esta terapia no tiene terapeutas.</h3>
								</div>
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>

@endsection