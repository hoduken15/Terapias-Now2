@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Asignar la terapia</h3>
		</div>
		<form role="form" method="POST" action="{{ route('admin.asigna', $terapia) }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="nombre">Nombre de la Terapia</label>
					<input 
						name="nombre" 
						type="text" 
						class="form-control" 
						id="nombre"
						placeholder="Ingresa el Nombre de la Terapia"
						value="{{$terapia->nombre}}" 
						disabled 
					>
				</div>
				<div class="form-group">
					<label for="terapeuta">Terapeuta</label>
					<select
						name="terapeuta"
						class="form-control"
						id="terapeuta" 
					>
						<option value="0">Selecciona un Usuario</option>
						@foreach($usuarios as $usuario)
							@if ($usuario->id_perfil == 2)
								<option value="{{$usuario->id}}">{{$usuario->name}}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="precio">Precio de la Terapia</label>
					<input 
						name="precio" 
						type="number" 
						class="form-control" 
						id="precio"
						placeholder="Ingresa el Precio de la Terapia"
					>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-success">Asignar Terapeuta</button>
				<a class="btn btn-secondary" href="javascript:history.back()">Volver</a>
			</div>
		</form>
	</div>
</div>

@endsection