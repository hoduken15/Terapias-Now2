@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

<div class="container">
	@if(isset($terapias))
		<div class="row">
			@foreach($terapias as $terapia)
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-2">
					<div class="small-box bg-secondary p-4">
						<div class="inner">
							<h3>{{ $terapia->nombre }}</h3>
							<p class="text-truncate">{{ $terapia->descripcion }}</p>
						</div>
						<a class="btn btn-primary small-box-footer" href="{{ route('terapias.edit',$terapia)}}">
							<i class="fas fa-edit"></i>
						</a>
						<a class="btn btn-primary small-box-footer" href="{{ route('terapias.destroy',$terapia)}}">
							<i class="fas fa-trash-alt"></i>
						</a>
					</div>
				</div>
			@endforeach
		</div>
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No existen terapias.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection