@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	@if(isset($users))
		<div class="row">
			@foreach($users as $terapeuta)
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="{{ route('users.verterapeuta', $terapeuta) }}">
						<div class="col-12">
							<div class="row border bg-secondary rounded">
								<div class="col-4 align-middle">
									@if(isset($terapeuta->imagen))
									    <img src="/imagenes/usuarios/{{$terapeuta->imagen}}" alt="Usuario-{{$terapeuta->id}}" style="width: 120px; height: 120px;" class="img-fluid rounded"/>
									@else
									    <img src="/imagenes/usuarios/default-user.jpg" alt="Usuario-{{$terapeuta->id}}" style="width: 120px; height: 120px;" class="img-fluid rounded"/>
									@endif
								</div>
								<div class="col-8">
									<h5 class="text-primary">{{ $terapeuta->name }}</h5>
									<small>{{ $terapeuta->email }}</small>
									<small>{{ $terapeuta->rut }}</small>
								</div>
							</div>
						</div>
					</a>
				</div>
			@endforeach
		</div>
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No existen terapeutas.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection