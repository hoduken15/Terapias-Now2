@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Actualizar un Usuario</h3>
		</div>
		<form role="form" method="GET" action="{{ route('users.adminupdate', $terapeuta) }}">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="nombre">Nombre del Usuario</label>
					<input 
						name="nombre" 
						type="text" 
						class="form-control" 
						id="nombre" 
						value="{{ $terapeuta->name }}"
						disabled
					>
				</div>
				<div class="form-group">
					<label for="tipo">Tipo de Perfil</label>
					<select
						name="tipo"
						class="form-control"
						id="tipo" 
					>
						<option value="2" selected>Terapeuta</option>
						<option value="1">Usuario</option>
					</select>
				</div>
				<div class="form-group">
					<label for="centro">Centro Terapeutico</label>
					<select
						name="centro"
						class="form-control"
						id="centro" 
					>
						<option value="0">Selecciona un Centro</option>
						@foreach($centros as $centro)	
							<option value="{{ $centro->id }}">{{ $centro->nombre }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-success">Actualizar Usuario</button>
				<a class="btn btn-secondary" href="javascript:history.back()">Volver</a>
			</div>
		</form>
	</div>
</div>

@endsection