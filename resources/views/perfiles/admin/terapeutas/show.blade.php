@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="row text-center">
		<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 p-2">
			<div class="small-box bg-secondary p-4">
				<div class="inner">
					@if(isset($usuario->imagen))
		                <img src="/imagenes/usuarios/{{$usuario->imagen}}" alt="Usuario-{{$usuario->id}}" style="width: 120px; height: 120px;" class="rounded img-fluid"/>
		            @else
		                <img src="/imagenes/usuarios/default-user.jpg" alt="Usuario-{{$usuario->id}}" style="width: 120px; height: 120px;" class="rounded img-fluid"/>
		            @endif
					<h3>{{ $usuario->name }}</h3>
					<div>
						<small>{{ $usuario->email }}</small>
					</div>
					<div>
						<small>{{ $usuario->rut }}</small>
					</div>
					@if(isset($usuario->Centros))
						<div>
							<small><i class="fa fa-map-marker"></i> {{ $usuario->Centros->nombre }}</small>
						</div>
					@endif
					<a href="{{ route('users.adminedit', $usuario) }}" type="button" class="btn btn-secondary btn-lg btn-block">Editar usuario</a>
					<form method="GET" action="{{ route('users.admindestroy', $usuario->id) }}">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-secondary btn-lg btn-block">Eliminar usuario</button>
					</form>
					<div class="col-12 text-center">
						<a class="btn btn-secondary" href="javascript:history.back()">Volver</a>
					</div>
				</div>
			</div>
		</div>
		@if($usuario->id_perfil === 1)
			<div class="col-lg-8 col-xs-12 col-sm-12 col-md-8">
				<h5>Comentarios del Usuario</h5>
				@if($usuario->PuntajeUsuario->isNotEmpty())
					<table class="table table-bordered">
						<tr>
							<th>Terapeuta</th>
							<th>Comentario</th>
							<th>Puntaje</th>
						</tr>
						@foreach($usuario->PuntajeUsuario as $comentarios)
							<tr>
								<td>{{ $comentarios->ComentariosTerapeuta->name }}</td>
								<td>{{ $comentarios->comentario }}</td>
								<td>{{ $comentarios->puntaje }}</td>
							</tr>
						@endforeach
					</table>
				@else
					<div class="text-center">
						<div class="col-lg-12 col-md col-xs-6 p-2 text-center">
							<div class="small-box bg-secondary p-4">
								<div class="inner">
									<h3>El usuario no ha dejado comentarios todavia.</h3>
								</div>
							</div>
						</div>
					</div>
				@endif
			</div>
		@elseif($usuario->id_perfil == 2)
			<div class="col-lg-8 col-xs-12 col-sm-12 col-md-8">
				@if($usuario->PuntajeTerapeuta->isNotEmpty())
					<h5>Comentarios del Terapeuta</h5>
					<table class="table table-bordered">
						<tr>
							<th>Usuario</th>
							<th>Comentario</th>
							<th>Puntaje</th>
						</tr>
						@foreach($usuario->PuntajeTerapeuta as $comentarios)
							<tr>
								<td>{{ $comentarios->ComentariosUsuario->name }}</td>
								<td>{{ $comentarios->comentario }}</td>
								<td>{{ $comentarios->puntaje }}</td>
							</tr>
						@endforeach
					</table>
				@else
					<div class="col-lg-3 col-xs-6 p-2 text-center">
						<div class="small-box bg-secondary p-4">
							<div class="inner">
								<h3>El terapeuta no tiene comentarios todavia.</h3>
							</div>
						</div>
					</div>
				@endif
			</div>
		@endif
	</div>
</div>

@endsection
