@extends('layout')

@section('title', 'Perfil')

@section('content')

<div class="container">
	<div class="row text-center">
		<div class="col-lg-4"></div>
		<div class="col-lg-4 col-xs-12 p-2">
			<div class="small-box bg-secondary p-4">
				<div class="inner">
					@if(isset($cliente->imagen))
		                <img src="/imagenes/usuarios/{{$cliente->imagen}}" alt="Usuario-{{$cliente->id}}" style="width: 120px; height: 120px;" class="rounded img-fluid"/>
		            @else
		                <img src="/imagenes/usuarios/default-user.jpg" alt="Usuario-{{$cliente->id}}" style="width: 120px; height: 120px;" class="rounded img-fluid"/>
		            @endif
					<h3>{{ $cliente->name }}</h3>
					<div>
						<small>{{ $cliente->email }}</small>
					</div>
					<div>
						<small>{{ $cliente->rut }}</small>
					</div>
					@if(isset($cliente->Centros))
						<div>
							<small><i class="fa fa-map-marker"></i> {{ $cliente->Centros->nombre }}</small>
						</div>
					@endif
				</div>
			</div>
		</div>
		<div class="col-lg-4"></div>
		@if($cliente->id_perfil === 1)
			<div class="col-12">
				<h5>Comentarios del Usuario</h5>
				@if($cliente->PuntajeUsuario->isNotEmpty())
					<table class="table table-bordered">
						<tr>
							<th>Terapeuta</th>
							<th>Comentario</th>
							<th>Puntaje</th>
						</tr>
						@foreach($cliente->PuntajeUsuario as $comentarios)
							<tr>
								<td>{{ $comentarios->ComentariosTerapeuta->name }}</td>
								<td>{{ $comentarios->comentario }}</td>
								<td>{{ $comentarios->puntaje }}</td>
							</tr>
						@endforeach
					</table>
				@else
					<div class="text-center">
						<div class="col-lg-12 col-md col-xs-6 p-2 text-center">
							<div class="small-box bg-secondary p-4">
								<div class="inner">
									<h3>El usuario no ha dejado comentarios todavia.</h3>
								</div>
							</div>
						</div>
					</div>
				@endif
			</div>
		@elseif($cliente->id_perfil == 2)
			<div class="col-12">
				@if($cliente->PuntajeTerapeuta->isNotEmpty())
					<h5>Comentarios del Terapeuta</h5>
					<table class="table table-bordered">
						<tr>
							<th>Usuario</th>
							<th>Comentario</th>
							<th>Puntaje</th>
						</tr>
						@foreach($cliente->PuntajeTerapeuta as $comentarios)
							<tr>
								<td>{{ $comentarios->ComentariosUsuario->name }}</td>
								<td>{{ $comentarios->comentario }}</td>
								<td>{{ $comentarios->puntaje }}</td>
							</tr>
						@endforeach
					</table>
				@else
					<div class="col-lg-3 col-xs-6 p-2 text-center">
						<div class="small-box bg-secondary p-4">
							<div class="inner">
								<h3>El terapeuta no tiene comentarios todavia.</h3>
							</div>
						</div>
					</div>
				@endif
			</div>
		@endif
		<div class="col-12 text-center">
			<a class="btn btn-secondary" href="javascript:history.back()">Volver</a>
		</div>
	</div>
</div>

@endsection