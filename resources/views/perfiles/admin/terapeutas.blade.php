@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

<div class="container">
	@if(isset($terapeutas))
		<div class="row">
			@foreach($terapeutas as $terapeuta)
				<div class="col-lg-4 col-xs-12 col-sm-12 col-md-6 p-4">
					<div class="row small-box bg-secondary p-3 rounded border border-primary">
						<div class="col-10">
							<div class="inner">
								<h3>{{ $terapeuta->name }}</h3>
								<small>{{ $terapeuta->email }}</small>
							</div>
							@if(isset($terapeuta->Centros))
								<div class="icon">
									<i class="fa fa-map-marker"></i> {{ $terapeuta->Centros->nombre }}
								</div>
							@endif
							@if(round($terapeuta->PuntajeTerapeuta->avg('puntaje'), 2) == 0)
								No tiene evaluaciones
							@else
								{{ round($terapeuta->PuntajeTerapeuta->avg('puntaje'), 2) }}
							@endif
							<div>
								<a href="{{ route('admin.user.show',$terapeuta)}}" class="small-box-footer">
									Ver más <i class="fa fa-arrow-circle-right"></i>
								</a>
							</div>
						</div>
						<div class="btn-group">
							<form id="delete-form" 
								action="{{ route('user.destroy',$terapeuta) }}" 
								method="POST" 
								style="display: none;"
							>{{ method_field('delete') }}{{ csrf_field() }}
							</form>
							<div>
								<a class="btn btn-primary small-box-footer" 
								href="#"
								onclick="event.preventDefault();
								document.getElementById('delete-form').submit();"
								><i class="fas fa-trash-alt"></i></a>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No existen terapeutas.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection