@extends('layout')

@section('title', 'Evaluación')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="box-header with-border col-8 align-left">
				<h3 class="box-title">Enviar un comentario!</h3>
			</div>
		</div>
		<form role="form" method="POST" action="{{ route('comentarios.guardar', $atencion) }}">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="terapeuta">Nombre del Terapeuta</label>
					<input 
						name="terapeuta" 
						type="terapeuta" 
						class="form-control" 
						id="terapeuta" 
						value="{{ $terapeuta->name }}"
						disabled
					>
				</div>
				<div class="form-group">
					<label for="terapia">Nombre de la Terapia</label>
					<input 
						name="terapia" 
						type="text" 
						class="form-control" 
						id="terapia" 
						value="{{ $terapia->nombre }}"
						disabled 
					>
				</div>
				<div class="form-group">
					<label for="horario">Fecha de la Atención</label>
					<input 
						name="horario" 
						type="text" 
						class="form-control" 
						id="horario" 
						value="{{ $horario->fecha }} - {{ $horario->hora }}"
						disabled
					>
				</div>
				<div class="form-group">
					<label for="precio">Precio de la Terapia</label>
					<input 
						name="precio" 
						type="number" 
						class="form-control" 
						id="precio" 
						value="{{ $atencion->precio }}"
						disabled
					>
				</div>
				<div class="form-group">
					<label for="comentario">Comentario</label>
					<input 
						name="comentario" 
						type="text" 
						class="form-control" 
						id="comentario" 
						placeholder="Ingresa un comentario" 
					>
				</div>
				<div class="form-group">
					<label for="puntaje">Puntaje</label>
						<div class="form-group required">
							<div class="col-sm-12" style="color: #EEE7F3;">
								<input class="star star-1" value="1" type="radio" id="star-1" name="star" style="display: none;" onclick="rate(1)" />
								<label class="star star-1" for="star-1"><span id="span-1" class="far fa-star fa-3x"></span></label>

								<input class="star star-2" value="2" type="radio" id="star-2" name="star" style="display: none;" onclick="rate(2)"/>
								<label class="star star-2" for="star-2"><span id="span-2" class="far fa-star fa-3x"></span></label>
								
								<input class="star star-3" value="3" type="radio" id="star-3" name="star" style="display: none;" onclick="rate(3)"/>
								<label class="star star-3" for="star-3"><span id="span-3" class="far fa-star fa-3x"></span></label>
								
								<input class="star star-4" value="4" type="radio" id="star-4" name="star" style="display: none;" onclick="rate(4)"/>
								<label class="star star-4" for="star-4"><span id="span-4" class="far fa-star fa-3x"></span></label>
								
								<input class="star star-5" value="5" type="radio" id="star-5" name="star" style="display: none;" onclick="rate(5)"/>
								<label class="star star-5" for="star-5"><span id="span-5" class="far fa-star fa-3x"></span></label>
							</div>
						</div>
				</div>
			</div>
			<div class="box-footer p-2">
				<button type="submit" class="btn btn-success">Enviar Comentario</button>
			</div>
		</form>
		<script type="text/javascript">
			function rate(id) {
				if (id == 1) {
					document.getElementById('span-1').style.color = '#BB5352';
					document.getElementById('span-2').style.color = '#EEE7F3';
					document.getElementById('span-3').style.color = '#EEE7F3';
					document.getElementById('span-4').style.color = '#EEE7F3';
					document.getElementById('span-5').style.color = '#EEE7F3';
				} else if (id == 2) {
					document.getElementById('span-1').style.color = '#BB5352';
					document.getElementById('span-2').style.color = '#BB5352';
					document.getElementById('span-3').style.color = '#EEE7F3';
					document.getElementById('span-4').style.color = '#EEE7F3';
					document.getElementById('span-5').style.color = '#EEE7F3';
				} else if (id == 3) {
					document.getElementById('span-1').style.color = '#BB5352';
					document.getElementById('span-2').style.color = '#BB5352';
					document.getElementById('span-3').style.color = '#BB5352';
					document.getElementById('span-4').style.color = '#EEE7F3';
					document.getElementById('span-5').style.color = '#EEE7F3';
				} else if (id == 4) {
					document.getElementById('span-1').style.color = '#BB5352';
					document.getElementById('span-2').style.color = '#BB5352';
					document.getElementById('span-3').style.color = '#BB5352';
					document.getElementById('span-4').style.color = '#BB5352';
					document.getElementById('span-5').style.color = '#EEE7F3';
				} else if (id == 5) {
					document.getElementById('span-1').style.color = '#BB5352';
					document.getElementById('span-2').style.color = '#BB5352';
					document.getElementById('span-3').style.color = '#BB5352';
					document.getElementById('span-4').style.color = '#BB5352';
					document.getElementById('span-5').style.color = '#BB5352';
				}
			}
		</script>
	</div>
</div>

@endsection