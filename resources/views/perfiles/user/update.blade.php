@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="box-header with-border col-8 align-left">
				<h3 class="box-title">Actualizar Datos</h3>
			</div>
			@if(auth()->user()->id_perfil != 2)
				<div class="box-header with-border col-4 align-right">
					<small class="box-title"><a href="{{ route('user.formulario', $user) }}">Quiero ser Terapeuta</a></small>
				</div>
			@endif
		</div>
		<form role="form" method="POST" action="{{ route('user.update', $user) }}" enctype="multipart/form-data">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="email">Email</label>
					<input 
						name="email" 
						type="email" 
						class="form-control" 
						id="email" 
						placeholder="Ingresa tu Email" 
						value="{{ $user->email }}"
					>
				</div>
				<div class="form-group">
					<label for="phone">Telefono</label>
					<input 
						name="phone" 
						type="number" 
						class="form-control" 
						id="phone" 
						placeholder="Ingresa tu Telefono" 
						value="{{ $user->phone }}"
					>
				</div>
				<div class="form-group">
					<label for="direccion">Dirección</label>
					<input 
						name="direccion" 
						type="text" 
						class="form-control" 
						id="direccion" 
						placeholder="Ingresa tu Dirección" 
						value="{{ $user->direccion }}"
					>
				</div>
				<div class="form-group">
					<label for="fecha_nacimiento">Fecha de Nacimiento</label>
					<input 
						name="fecha_nacimiento" 
						type="date" 
						class="form-control" 
						id="fecha_nacimiento" 
						value="{{ $user->fecha_nacimiento }}"
					>
				</div>
				<div class="form-group">
					<label for="imagen">Foto de Perfil</label>
					<input 
						name="imagen" 
						type="file" 
						class="form-control" 
						id="imagen" 
					>
				</div>
				{{-- <div class="form-group">
					<label for="ImagenPerfil">Imagen de Perfil</label>
					<input name="ImagenPerfil" type="file" id="ImagenPerfil">
				</div> --}}
			</div>
			<div class="box-footer p-2">
				<button type="submit" class="btn btn-success">Actualizar</button>
			</div>
		</form>
	</div>
</div>

@endsection