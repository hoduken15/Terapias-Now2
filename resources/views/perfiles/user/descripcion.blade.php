@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="box-header with-border col-8 align-left">
				<h3 class="box-title">Añadir descripción</h3>
			</div>
		</div>
		<form role="form" method="GET" action="{{ route('user.descripcionupdate', $user) }}">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="descripcion">Descripción</label>
					<input 
						name="descripcion" 
						type="longtext" 
						class="form-control" 
						id="descripcion" 
						placeholder="Ingresa una Descripción" 
						maxlength="250"
						value="{{$user->descripcion}}"
					>
				</div>
			</div>
			<div class="box-footer p-2">
				<button type="submit" class="btn btn-success">Enviar</button>
			</div>
		</form>
	</div>
</div>

@endsection