@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

<div class="container">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Solicitar convertirse en Terapeuta</h3>
		</div>
		<form role="form" method="POST" action="{{ route('user.solicita', $user) }}">
			{{ csrf_field() }}
			<div class="box-body">
				<hr>
				<div class="form-group">
					<label for="descripcion">Descripción</label><br>
					<small>Entrega una descripción detallada del porque quieres ser terapeuta.</small>
					<input 
						name="descripcion" 
						type="text" 
						class="form-control" 
						id="descripcion" 
					>
				</div>
				<hr>
				<div class="form-group">
					<label for="terapias">Terapias</label><br>
					<small>Selecciona una terapia para empezar.</small>
					<div class="form-group">
						<select class="form-control" name="terapias">
							<option>Selecciona una terapia</option>
							@foreach($terapias as $terapia)
								<option value="{{$terapia->id}}">{{ $terapia->nombre }}</option>
							@endforeach
						</select>
					</div>
					<hr>
				</div>
				<div class="form-group">
					<label for="precio">Precio</label><br>
					<small>Indica el precio al cual quieres dar la terapia.</small>
					<input 
						name="precio" 
						type="number" 
						class="form-control" 
						id="precio" 
					>
				</div>
			</div>
			<div class="box-footer p-2">
				<button type="submit" class="btn btn-success">Solicitar</button>
			</div>
		</form>
	</div>
</div>

@endsection