@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	@if(!$comentarios->isEmpty())
		<div class="row justify-content-center">
		  	@foreach($comentarios as $comentario)
			  	<div class="col-12 col-sm-12 col-md-8 col-lg-8">
			        <div class="row bg-secondary border rounded" style="margin-bottom: 5px; margin-right: 5px; margin-left: 5px;">
			    		<div class="col-2">
			    			<img src="/imagenes/usuarios/{{$comentario->ComentariosTerapeuta->imagen}}" alt="Usuario-{{$comentario->ComentariosTerapeuta->id}}" class="rounded img-responsive" style="width: 120px; height: 120px;" />
			    		</div>
			    		<div class="col-10">
			    			<div class="col-12">
			    				<div class="row" style="color: #5F1961;">
			    					<div class="col-4">
			    						Terapeuta
			    					</div>
			    					<div class="col-8">
			    						{{ $comentario->ComentariosTerapeuta->name }}
			    					</div>
			    				</div>
			    			</div>
			    			<div class="col-12">
			    				<div class="row text-texto">
			    					<div class="col-4">
			    						Puntaje
			    					</div>
			    					<div class="col-8">
			    						{{ $comentario->puntaje }}
			    					</div>
			    				</div>
			    			</div>
			    			<div class="col-12">
			    				<div class="row text-texto">
			    					<div class="col-4">
			    						Comentario
			    					</div>
			    					<div class="col-8">
			    						{{ $comentario->comentario }}
			    					</div>
			    				</div>
			    			</div>
			    		</div>
			        </div>
			    </div>
			@endforeach
		</div>
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No te han dejado comentarios todavia.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection