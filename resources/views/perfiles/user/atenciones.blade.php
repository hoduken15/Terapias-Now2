@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	@if(!$atenciones->isEmpty())
		<div class="row justify-content-center">
		  	@foreach($atenciones as $atencion)
			  	<div class="col-12 col-sm-12 col-md-8 col-lg-8">
				    <div class="row bg-secondary border border-white rounded">
						<div class="col-4 col-md-4 col-lg-4 col-sm-4">
							<a href="{{route('search.showterapeuta', $atencion->Terapeuta)}}">
								<img src="/imagenes/usuarios/{{$atencion->Terapeuta->imagen}}" style="height: 100px; width: 100px;" alt="Terapeuta-{{$user->id}}" class="img-responsive rounded"/>
							</a>
						</div>
						<div class="col-8 col-md-8 col-lg-8 col-sm-8" style="padding-right: 0px; padding-left: 0px;">
							<div class="col-12">
								<div class="row" style="color: #5F1961;">
									<div class="col-4">
										Terapia
									</div>
									<div class="col-8">
										{{ $atencion->Terapia->nombre }}
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="row text-texto">
									<div class="col-4">
										Terapeuta
									</div>
									<div class="col-8">
										{{ $atencion->Terapeuta->name }}
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="row text-texto">
									<div class="col-4">
										Hora
									</div>
									<div class="col-8">
										{{ $atencion->Horario->fecha }} {{ $atencion->Horario->hora }}
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="row text-texto">
									<div class="col-4">
										Precio
									</div>
									<div class="col-8">
										${{ $atencion->precio }}.-
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="row text-texto">
									<div class="col-4">
										Estado
									</div>
									<div class="col-8">
										{{ $atencion->estado }}
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xl-12 justify-content-center">
							<a href="{{ route('comentarios.comentar',$atencion) }}" class="btn btn-md btn-block btn-success ">
								<i class="far fa-check-circle"></i> Dejar un comentario
							</a>
						</div>
				    </div>
			    </div>
			@endforeach
		</div>
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No tienes atenciones.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection