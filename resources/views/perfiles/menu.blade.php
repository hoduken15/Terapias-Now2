@if($user->id_perfil == '2')
    <div class="container">
        <ul class="nav nav-tabs justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('terapias.show', $user) }}">Terapias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('perfil.comentarios', $user) }}">Comentarios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('perfil.horario', $user) }}">Horario</a>
            </li>
        </ul>
    </div>
@elseif($user->id_perfil == '3')
    <div class="container">
        <ul class="nav nav-tabs justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.terapeutas') }}">Usuarios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.terapias') }}">Terapias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('noticias.index') }}">Noticias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('solicitudes.index') }}">Solicitudes</a>
            </li>
        </ul>
    </div>
@elseif($user->id_perfil == '1')
    <div class="container">
        <ul class="nav nav-tabs justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('atenciones.show', auth()->user()) }}">Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('comentarios.showuser', auth()->user()) }}">Comentarios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('comentarios.showterapeuta', auth()->user()) }}">Evaluaciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('user.edit', auth()->user()) }}">Actualizar perfil</a>
            </li>
        </ul>
    </div>
@endif