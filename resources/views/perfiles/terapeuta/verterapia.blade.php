@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="box-header with-border col-8 align-left">
				<h3 class="box-title">Añadir una descripcion propia para la terapia.</h3>
			</div>
		</div>
		<form role="form" method="POST" action="{{ route('terapia.userupdate', $terapia) }}">
			{{ method_field('GET') }}
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="terapia">Terapia</label>
					<input 
						name="terapia" 
						type="text" 
						class="form-control" 
						id="terapia" 
						value="{{ $terapia->nombre }}"
						disabled
					>
				</div>
				<div class="form-group">
					<label for="descripcion">Descripción</label>
					<textarea  
						name="descripcion" 
						type="text"
						cols="40" 
       					rows="5" 
						class="form-control" 
						id="descripcion"
						required 
					>
					{{($user->TerapiasUser->where('terapias_id', $terapia->id)[0]->descripcion == "") ? $terapia->descripcion : $user->TerapiasUser->where('terapias_id', $terapia->id)[0]->descripcion }}
					</textarea>
				</div>
			</div>
			<div class="box-footer p-2">
				<button type="submit" class="btn btn-success">Actualizar</button>
			</div>
		</form>
	</div>
</div>


@endsection