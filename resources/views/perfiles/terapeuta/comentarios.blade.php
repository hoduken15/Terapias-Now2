@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

<div class="container">
	@if(count($comentarios) > 0)
		<div class="row justify-content-center">
		  	@foreach($comentarios as $comentario)
			  	<div class="col-12 col-sm-12 col-md-8 col-lg-8">
		  		    <div class="row bg-secondary border rounded" style="margin-bottom: 5px; margin-right: 5px; margin-left: 5px;">
		  				<div class="col-4 col-lg-4 col-md-4 col-sm-4">
		  					<img src="/imagenes/usuarios/{{$comentario->ComentariosUsuario->imagen}}" alt="Usuario-{{$comentario->ComentariosUsuario->id}}" class="rounded img-responsive" style="width: 120px; height: 120px;" />
		  				</div>
		  				<div class="col-8 col-lg-8 col-md-8 col-sm-8">
		  					<div class="col-12">
		  						<div class="row" style="color: #5F1961;">
		  							<div class="col-4">
		  								Cliente
		  							</div>
		  							<div class="col-8">
		  								{{ $comentario->ComentariosUsuario->name }}
		  							</div>
		  						</div>
		  					</div>
		  					<div class="col-12">
		  						<div class="row text-texto">
		  							<div class="col-4">
		  								Puntaje
		  							</div>
		  							<div class="col-8">
		  								{{ $comentario->puntaje }}
		  							</div>
		  						</div>
		  					</div>
		  					<div class="col-12">
		  						<div class="row text-texto">
		  							<div class="col-4">
		  								Comentario
		  							</div>
		  							<div class="col-8">
		  								{{ $comentario->comentario }}
		  							</div>
		  						</div>
		  					</div>
		  					<div class="col-12">
		  						<div class="row text-texto">
		  							<div class="col-4">
		  								Fecha
		  							</div>
		  							<div class="col-8">
		  								{{ $comentario->created_at->format('d-m-Y') }}
		  							</div>
		  						</div>
		  					</div>
		  				</div>
		  		    </div>
	  		    </div>
			@endforeach
		</div>
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">Este terapeuta todavia no cuenta con comentarios.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection