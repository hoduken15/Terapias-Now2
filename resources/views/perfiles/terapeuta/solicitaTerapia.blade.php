@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="box-header with-border col-8 align-left">
				<h3 class="box-title">Solicitar una Terapia Existente</h3>
			</div>
		</div>
		<form role="form" method="POST" action="{{ route('terapias.solicitar', $user) }}">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="terapia">Terapias</label>
					<select
						name="terapia"
						class="form-control"
						id="terapia" 
					>
						<option>Seleccione una Terapia</option>
						@foreach($terapias as $terapia)
							@if (!$user->TerapiasUser->keyBy('terapias_id')->has($terapia->id))
						    	<option value="{{$terapia->id}}">{{ $terapia->nombre}}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="descripcion">Descripción</label><br>
					<input 
						name="descripcion" 
						type="text" 
						class="form-control" 
						id="descripcion"
						placeholder="Ingresa una descripción de tu solicitud" 
					>
				</div>
				<div class="form-group">
					<label for="precio">Precio de la Terapia</label>
					<input 
						name="precio" 
						type="number" 
						class="form-control" 
						id="precio" 
						placeholder="Ingresa el precio de la Terapia" 
						required 
					>
				</div>
			</div>
			<div class="box-footer p-2">
				<button type="submit" class="btn btn-success">Solicitar</button>
			</div>
		</form>
		<hr>
		<div class="row">
			<div class="box-header with-border col-8 align-left">
				<h3 class="box-title">Solicitar una Nueva Terapia</h3>
			</div>
		</div>
		<form role="form" method="POST" action="{{ route('terapias.sugerir', $user) }}">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="nombre">Nombre de la Terapia</label>
					<input 
						name="nombre" 
						type="text" 
						class="form-control" 
						id="nombre" 
						placeholder="Ingresa el nombre de la Terapia" 
						required
					>
				</div>
				<div class="form-group">
					<label for="descripcion">Descripción de la Terapia</label>
					<input 
						name="descripcion" 
						type="text" 
						class="form-control" 
						id="descripcion" 
						placeholder="Ingresa la descripción de la Terapia" 
						required
					>
				</div>
				<div class="form-group">
					<label for="precio">Precio de la Terapia</label>
					<input 
						name="precio" 
						type="number" 
						class="form-control" 
						id="precio" 
						placeholder="Ingresa el precio de la Terapia" 
						required
					>
				</div>
			</div>
			<div class="box-footer p-2">
				<button type="submit" class="btn btn-success">Solicitar</button>
			</div>
		</form>
	</div>
</div>

@endsection