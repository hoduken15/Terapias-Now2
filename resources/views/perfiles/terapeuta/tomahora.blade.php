@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

<div class="container">
	
	<div class="box box-warning">
		<div class="box-header with-border">
			<h3 class="box-title">Selecciona una Hora</h3>
		</div>
		<div class="box-body">
			<form role="form" method="POST" action="{{ route('atenciones.store') }}">
				@csrf
				<div class="form-group">
					<label>Terapeuta</label>
					<input type="text" class="form-control" value="{{ $user->name }}" disabled>
					<input type="text" class="form-control" name="terapeuta" hidden value="{{ $user->id }}">
				</div>
				<div class="form-group">
					<label>Terapia</label>
					<input type="text" class="form-control" value="{{ $terapia->nombre }}" disabled>
					<input type="text" class="form-control" name="terapia" hidden value="{{ $terapia->id }}">
				</div>
				<div class="form-group">
					<label>Precio</label>
					@foreach($user->User as $pivot)
						@if($pivot->pivot->terapias_id == $terapia->id)
							<input type="text" class="form-control" value="{{ $pivot->pivot->precio }}" disabled>
							<input type="text" class="form-control" name="precio" hidden value="{{ $pivot->pivot->precio }}">
						@endif
					@endforeach
				</div>
				<div class="form-group">
					<label>Horas</label>
					@if(isset($error))
						<div class="container alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="fas fa-times"></i> {{ $error }}</h4>
						</div>
					@endif
					<select class="form-control" name="hora">
						<option>Selecciona una hora</option>
						@foreach($horas as $hora)
							<option value="{{$hora->id}}">{{ $hora->fecha . " - " . $hora->hora }}</option>
						@endforeach
					</select>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Solicitar Hora</button>
				</div>
			</form>
		</div>
	</div>

</div>

@endsection