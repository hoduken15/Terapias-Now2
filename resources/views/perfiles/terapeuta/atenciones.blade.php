@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container">
	@if(!$atenciones->isEmpty())
	  	@foreach($atenciones as $atencion)
	        <div class="row bg-secondary border rounded" style="margin-bottom: 5px; margin-right: 5px; margin-left: 5px;">
	    		<div class="col-3">
	    			<a href="{{route('search.showterapeuta', $atencion->Cliente)}}">
	    				<img src="/imagenes/usuarios/{{$atencion->Cliente->imagen}}" style="height: 100px; width: 100px;" alt="Terapeuta-{{$user->id}}" class="img-responsive rounded"/>
	    			</a>
	    		</div>
	    		<div class="col-9">
	    			<div class="col-12">
	    				<div class="row" style="color: #5F1961;">
	    					<div class="col-4">
	    						Terapia
	    					</div>
	    					<div class="col-8">
	    						{{ $atencion->Terapia->nombre }}
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-12">
	    				<div class="row text-texto">
	    					<div class="col-4">
	    						Cliente
	    					</div>
	    					<div class="col-8">
	    						{{ $atencion->Cliente->name }}
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-12">
	    				<div class="row text-texto">
	    					<div class="col-4">
	    						Hora
	    					</div>
	    					<div class="col-8">
	    						{{ $atencion->Horario->fecha }} {{ $atencion->Horario->hora }}
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-12">
	    				<div class="row text-texto">
	    					<div class="col-4">
	    						Precio
	    					</div>
	    					<div class="col-8">
	    						${{ $atencion->precio }}.-
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-12">
	    				<div class="row text-texto">
	    					<div class="col-4">
	    						Estado
	    					</div>
	    					<div class="col-8">
	    						{{ $atencion->estado }}
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xl-12 justify-content-center">
	    			<a href="{{ route('comentarios.comentaterapeuta',$atencion) }}" class="btn btn-md btn-block btn-success ">
	    				<i class="far fa-check-circle"></i> Dejar un comentario
	    			</a>
	    		</div>
	        </div>
		@endforeach
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No tienes atenciones.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection