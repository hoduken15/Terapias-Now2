@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

<div class="container">
	@if(count($user->User) > 0)
	  	@foreach($user->User as $terapias)
			{{-- <a href="{{ (auth()->user()->id == $user->id) ? route('perfil.miterapia', ['id' => $user->id, 'terapia' => $terapias->id]) : route('perfil.hora', ['id' => $user->id, 'terapia' => $terapias->id])}}"> --}}
			@if (auth()->user()->id == $user->id)
			    <div class="card text-center border bg-secondary">
			      	<div class="card-body">
			        	<h5 class="card-title text-primary">{{ $terapias->nombre }}</h5>
			        	<p class="card-text text-truncate text-primary">
			        		{{$terapias->descripcion}}
			        	</p>
			      	</div>
			    </div>
			@else
				<a href="{{ route('perfil.hora', ['id' => $user->id, 'terapia' => $terapias->id]) }}">
				    <div class="card text-center border bg-secondary" style="margin-bottom: 10px;">
				      	<div class="container" >
				      		<div class="row">
				      			<div class="col-2" style="padding-left: 0px; padding-right: 0px;">
				      				@if(isset($terapias->imagen))
				      					<img src="/imagenes/terapias/{{$terapias->imagen}}" alt="Terapia-{{$terapias->id}}" class="card-img-top rounded img-fluid"/>
				      				@else
				      					<img src="/imagenes/terapias/default.jpg" alt="Terapia-{{$terapias->id}}" class="card-img-top rounded img-fluid"/>
				      				@endif
				      			</div>
				      			<div class="col-10">
				      				<div class="row">
				      					<div class="col-12 bg-primary" style="color: #EEE7F3; text-align: left;">
				      						<h5 class="card-title">{{ $terapias->nombre }}</h5>
				      					</div>
				      					<div class="col-12">
				      						<p class="card-text text-truncate">
				      							{{$terapias->descripcion}}
				      						</p>
				      					</div>
				      				</div>
				      			</div>
				      		</div>
{{-- 				        	<h5 class="card-title">{{ $terapias->nombre }}</h5>
				        	<p class="card-text text-truncate">
				        		{{$terapias->descripcion}}
				        	</p> --}}
				      	</div>
				    </div>
				</a>
			@endif
		@endforeach
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">Este terapeuta todavia no cuenta con terapias para impartir.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection