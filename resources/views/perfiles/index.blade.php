@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

@if($user->descripcion != null)
	<div class="container text-center">
		<div class="card text-center bg-secondary">
			<div class="card-body bg-secondary">
				<h3>{{ $user->descripcion }}</h3>
			</div>
			@if(auth()->user()->id === $user->id)
				<a href="{{ route('user.descripcion', $user) }}">Actualizar Descripción</a>
			@endif
		</div>
	</div>
@elseif(auth()->user()->id === $user->id)
	<div class="container text-center">
		<div class="card text-center">
			<div class="card-body">
				<h3>No tienes descripción todavia, actualiza tu perfil y añade una!</h3>
			</div>
			<a href="{{ route('user.descripcion', $user) }}">Agregar Descripción</a>
		</div>
	</div>
@else
	<div class="container text-center">
		<div class="card text-center">
			<div class="card-body">
				<h3>Este usuario no tiene descripción.</h3>
			</div>
		</div>
	</div>
@endif

@endsection