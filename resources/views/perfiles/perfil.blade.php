<div class="col-12">
    <div class="row">
        <div class="col-6 text-right">
            @if(isset($user->imagen))
                <img src="/imagenes/usuarios/{{$user->imagen}}" alt="Usuario-{{$user->id}}" style="width: 120px; height: 120px;" class="rounded img-responsive"/>
            @else
                <img src="/imagenes/usuarios/default-user.jpg" alt="Usuario-{{$user->id}}" style="width: 120px; height: 120px;" class="rounded img-responsive"/>
            @endif
        </div>
        <div class="col-6 text-primary">
            <div class="row">
                <i class="fas fa-user"></i>
                <a href="{{ route('search.showterapeuta', $user) }}">
                    <h4>{{ $user->name }}</h4>
                </a>
            </div>
            @if(isset($user->Centros) || $user->Centros != null)
                <div class="row">
                    <label><i class="fa fa-map-marker"></i> {{ $user->Centros->nombre }}</label>
                </div>
            @endif
            @if(isset($user->fecha_nacimiento) || $user->fecha_nacimiento != null)
                <div class="row">
                    <label><i class="fa fa-address-card"></i> 
                        {{ \Carbon\Carbon::parse($user->fecha_nacimiento)->age }}
                         años</label>
                </div>
            @endif
            <div class="row">
                <label><i class="fa fa-envelope"></i> {{ $user->email }}</label>
            </div>
            {{-- @if($user->id_perfil == '2' && isset($puntaje)) 
                {{ round($puntaje, 2) }}
                <a href="#">
                    <span class="fa fa-star"></span>
                </a>
            @endif --}}
        </div>
    </div>
</div>