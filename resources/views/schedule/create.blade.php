@extends('layout')

@section('title', 'Horario')

@section('content')

@if(isset($horas) || isset($dia))
	<div class="container p-2">
		<form method="POST" action="{{ route('schedule.store')}}">
			{{ csrf_field() }}
			<div class="box-header with-border text-center">
				<h3 class="box-title">Ingresar Horario</h3>
			</div>
			<div class="row">
				<div class="col-12">
					@foreach($dia as $d)
						<div class="row border p-2">
							<div class="col-lg-2 col-md-2 col-sm-12 text-center">
								<p>{{ $d }}</p>
							</div>
							<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
								<div class="row d-flex justify-content-between">
									@foreach($horas as $hora)
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 rounded border" style="margin-bottom: 2px; margin-right: 2px; width: auto;" id="label-{{ $d }}.{{ $hora }}" >
											<input id="{{ $d }}.{{ $hora }}" onclick="isChecked('{{ $d }}.{{ $hora }}');" style="display: none;" class="icheckbox_flat-green" type="checkbox" value="{{ $hora }}" name="{{ $d }}[]">
											<label class="form-check-label" for="{{ $d }}.{{ $hora }}">{{ $hora }}</label>
										</div>
									@endforeach
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<input type="submit" class="btn btn-primary" value="Ingresar"> 
			</div>
		</form>
	</div>
@else
	<div class="container-fluid p-3">
		<div class="card text-center bg-secondary">
	      	<div class="card-body">
	        	<h5 class="card-title">{{ $mensaje }}</h5>
	      	</div>
	      	<a href="{{ route('schedule.index') }}">Volver</a>
		</div>
	</div>
@endif
<script type="text/javascript">
	function isChecked(id) {
		var checkbox = document.getElementById(id);
		var	div = document.getElementById('label-' + id);
		// alert(checkbox);
		if (checkbox.checked == true) {
			// alert(label);
			div.style.backgroundColor = "green";
			div.style.color = "white";
		} else {
			div.style.backgroundColor = "white";
			div.style.color = "black";
		}
	}
</script>
@endsection