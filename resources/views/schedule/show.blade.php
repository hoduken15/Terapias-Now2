@extends('layout')

@section('title', 'Perfil')

@section('content')

@include('perfiles.parcial')

<div class="container">
	@if(isset($horarios))
		<div class="container p-2">
			<div class="box-header with-border text-center">
				<h3 class="box-title">Horario</h3>
				<div>
					<span style="background-color: green; width: 5px; height: 5px;"></span>
					<small>En color verde las horas disponibles!</small>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					@foreach($dias as $d)
						<div class="row border p-2">
							<div class="col-lg-2 col-md-2 col-sm-12 text-center">
								<p>{{ $d }}</p>
							</div>
							<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
								<div class="row d-flex justify-content-left">
									@foreach($horas as $hora)
										{{-- {{dd($horarios)}} --}}
										@if ($horarios->has([$d . "|" . $hora]))
											<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 rounded border" style="margin-bottom: 2px; margin-right: 2px; width: auto; background-color: #CEF2B8;">
												<label class="form-check-label">{{ $hora }}</label>
											</div>
										@else
											<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 rounded border" style="margin-bottom: 2px; margin-right: 2px; width: auto;">
												<label class="form-check-label">{{ $hora }}</label>
											</div>
										@endif
									@endforeach
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@else
		<div class="container-fluid p-3">
			<div class="card text-center">
		      	<div class="card-body">
		        	<h5 class="card-title">{{ $mensaje }}</h5>
		      	</div>
		      	<a href="{{ route('schedule.index') }}">Volver</a>
			</div>
		</div>
	@endif

</div>

@endsection