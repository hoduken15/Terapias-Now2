@extends('layout')

@section('title', 'Horario')

@section('content')

@if(isset($mensaje))
	<div class="container alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
	</div>
@endif

<div class="container text-center">
	<a href="{{ route('perfil.horario', auth()->user()->id) }}">Ver mi calendario.</a><br>
	<a href="{{ route('schedule.create') }}">Crear mi calendario.</a><br>
	<a href="{{-- {{ route('schedule.update') }} --}}">Modificar mi calendario.</a><br>
</div>


@endsection