@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container text-center">
    <div class="row">
        <div class="text-center bg-primary col-12" style="color: #EEE7F3;">
            <h4>Noticias</h4>
        </div>
        <div class="container">
            <hr>
            <div class="row">
                @foreach($noticia as $noticiaItem)
                    <div class="card mb-3 col-sm-12 col-xs-12 col-md-6 col-lg-6 bg-secondary border border-white" style="border: none; margin-bottom: 2px;">
                        <a href="{{ route('home.noticias',$noticiaItem) }}" style="outline: none;">
                            <div class="row no-gutters">
                                <div class="col">
                                    <img src="/imagenes/noticias/{{$noticiaItem->imagen}}" alt="Noticia-{{$noticiaItem->id}}" class="img-fluid rounded"/>
                                </div>
                                <div class="col-8">
                                    <div class="card-body text-left">
                                        <div>
                                                <h5 class="card-title noticia-titulo"> {{ $noticiaItem->titulo }} </h5>
                                        </div>
                                        <div class="noticia-fecha" style="z-index: 1;">
                                            <p class="card-text"><small class="text-muted">{{ $noticiaItem->fecha }}</small></p>
                                        </div>
                                        <p class="card-text noticia-texto">{{$noticiaItem->detalle}}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
