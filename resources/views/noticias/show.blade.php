@extends('layout')

@section('title', $noticia->titulo)

@section('content')

<div class="container">
	<div class="d-flex row justify-content-center">
		<div class="col-12 d-flex">
			<div class="card" style="width: 18rem;">
				<img src="/imagenes/noticias/{{$noticia->imagen}}" alt="Noticia-{{$noticia->id}}" class="img-fluid rounded"/>
				<div class="card-body text-center">
					<h5 class="card-title">{{ $noticia->titulo }}</h5>
					<p class="card-text">{{ $noticia->detalle }}</p>
					<a href="/home" class="btn btn-primary">Volver</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
