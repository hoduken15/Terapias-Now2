<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Terapias Now</title>
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/sw.js') }}"></script>
        <script src="https://kit.fontawesome.com/201b54bf45.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div id="app" class="d-flex flex-column h-screen justify-content-between">
            <div class="d-flex flex-column h-screen justify-content-between">
                <header class="navbar-header" id="header" style="position: fixed; top: 0; z-index: 2; width: 100%;">
                    <div class="fixed bg-secondary" style="width: all;">
                        <div id="logo" class="p-2 navbar navbar-light bg-primary shadow-sm justify-content-between" style="padding-right: unset; padding-left: unset;">
                            <a id="openNav" href="javascript:void(0)" class="navbar-brand" style="width: 50px; height: 30px; outline: none;" onclick="toggleNav()"><i class="fas fa-bars" style="z-index: 3;"></i></a>
                            <input type="text" id="estadoNav" value="close" style="display: none">
                            <router-link class="navbar-brand" style="color: #EEE7F3; font-size: 20px;" to="/">
                                {{config('app.name')}}
                            </router-link>
                            <router-link class="navbar-brand" to="perfil">
                                @if(isset(auth()->user()->imagen))
                                    <img src="/imagenes/usuarios/{{auth()->user()->imagen}}" alt="Usuario-{{auth()->user()->id}}" style="width: 40px; height: 40px;" class="rounded-circle"/>
                                @else
                                    <img src="/imagenes/usuarios/default-user.jpg" alt="Usuario-{{auth()->user()->id}}" style="width: 40px; height: 40px;" class="rounded-circle"/>
                                @endif
                            </router-link>
                        </div>
                        <inputbusqueda></inputbusqueda>
                    </div>
                    <div id="mySidenav" class="sidenav bg-secondary fixed-left">
                        @auth
                            <router-link class="nav-link" to="perfil">Perfil</router-link>
                            <router-link class="nav-link" to="/">Noticias</router-link>
                            <router-link class="nav-link" to="notificaciones">Notificaciones</router-link>
                            @if(auth()->user()->id_perfil == '1')
                                <router-link class="nav-link" :to="{name: 'atenciones-show', params: { id: {{ auth()->user()->id }} }}">Mis Horas</router-link>
                                <router-link class="nav-link" :to="{name: 'comentarios-showuser', params: { id: {{ auth()->user()->id }} }}">Mis Calificaciones</router-link>
                            @elseif(auth()->user()->id_perfil == '2')
                                <router-link class="nav-link" :to="{name: 'terapias-show', params: { id: {{ auth()->user()->id }} }}">Mis Terapias</router-link>
                                <router-link class="nav-link" :to="{name: 'perfil-horario', params: { id: {{ auth()->user()->id }} }}">Mi Horario</router-link>
                                <router-link class="nav-link" :to="{name: 'perfil-comentarios', params: { id: {{ auth()->user()->id }} }}">Mis Calificaciones</router-link>
                                <router-link class="nav-link" :to="{name: 'atenciones-ver', params: { id: {{ auth()->user()->id }} }}">Mis Atenciones</router-link>
                                <router-link class="nav-link" :to="{name: 'terapias-anadir', params: { id: {{ auth()->user()->id }} }}">Solicitar Terapias</router-link>
                                <router-link class="nav-link" :to="{name: 'horas-ver', params: { id: {{ auth()->user()->id }} }}">Mis Horas</router-link>
                            @elseif(auth()->user()->id_perfil == '3')
                                <router-link class="nav-link" :to="{name:'admin-usuarios'}">Usuarios</router-link>
                                <router-link class="nav-link" :to="{name:'admin-terapias'}">Terapias</router-link>
                                <router-link class="nav-link" :to="{name:'noticias-index'}">Noticias</router-link>
                                <router-link class="nav-link" :to="{name:'solicitudes-index'}">Solicitudes</router-link>
                            @endif
                            <router-link class="nav-link" :to="{name: 'user-edit', params: { id: {{ auth()->user()->id }} }}">Actualizar mi perfil</router-link>
                            <hr>
                            <router-link class="nav-link" to="#" onclick="document.getElementById('logout-form').submit();" >Cerrar Sesión</router-link>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                        @else
                            <router-link class="nav-link" to="{{-- {{route('login')}} --}}">Iniciar Sesión</router-link>
                        @endauth
                    </div>
                </header>
                <main id="main" class="align-top bg-fondo p-2" style="height: 100%; top: 115px; position: absolute; bottom: 0; width: 100%;">
                    <div class="container d-flex p-2">

                        <router-view></router-view>

                    </div>
                </main>
                <footer id="footer" style="position: fixed; bottom: 0; z-index: 2; width: 100%;" class="bg-primary text-black-50 text-center py-3 shadow">
                    {{ config('app.name')}} | Copyright @ {{ date('Y') }}
                </footer>
            </div>
        </div>
    </body>
    <script>
        function toggleNav() {
            var x = document.getElementById("estadoNav").value;
            if(x == "close"){
                document.getElementById("mySidenav").style.width = "150px";
                document.getElementById("estadoNav").value = "open";
            } else {
                document.getElementById("mySidenav").style.width = "0";
                document.getElementById("estadoNav").value = "close";
            }
        }
    </script>
</html>
<script>
    import Inputbusqueda from "../../js/views/inputbusqueda";
    export default {
        components: {Inputbusqueda}
    }
</script>
