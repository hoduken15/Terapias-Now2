@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 15%;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="container p-2">
                <p class="titulo-login text-center text-primary h1">
                    Terapias
                </p>
                <p class="titulo-login text-center text-primary h1">
                    Now
                </p>
                <div class="card-body">
                    <form method="POST" class="text-center" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="email" type="email" class="form-control bg-secondary @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="E-Mail" required autocomplete="off" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="password" type="password" class="form-control bg-secondary @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary text-white">
                                    Ingresar
                                </button>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        ¿Olvidaste tu Contraseña?
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                    <div class="text-center">
                        @guest
                            @if (Route::has('register'))
                                <a class="btn btn-link" href="{{ route('register') }}">¿No tienes cuenta? Registrate Aquí!</a>
                            @endif
                        @endguest
                    </div>
                    {{-- <div class="form-group row">
                        <div class="col-xs-12 col-sm-12 p-2">
                            <div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div>
                            <div class="g-signin2" data-onsuccess="onSignIn"></div>
                            <a href="#" onclick="signOut();">Sign out</a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="text-center">
    <div class="d-flex flex-column">
        <small class="text-primary"><strong>Por</strong></small>
        <div>
            <img style="justify-content: center;" src="logos/kosyka.PNG" height="39" width="100">
        </div>
    </div>
</div>
@endsection
