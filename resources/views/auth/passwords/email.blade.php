@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 15%;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="container p-2">
                <p class="titulo-login text-center text-primary h1">
                    Terapias
                </p>
                <p class="titulo-login text-center text-primary h1">
                    Now
                </p>
                <div class="card-body">
                    <form method="POST" class="text-center" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-12">
                                <input id="email" type="email" class="form-control bg-secondary @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="E-Mail" required autocomplete="off" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary text-white">
                                    Reiniciar Contraseña
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
