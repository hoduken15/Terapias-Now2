<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title', 'Terapias Now')</title>
		<link rel="stylesheet" href="{{ mix('css/app.css') }}">
		<script src="{{ asset('js/app.js') }}" defer></script>
		<script src="{{ asset('js/sw.js') }}"></script>
		{{-- <script src="https://kit.fontawesome.com/201b54bf45.js" crossorigin="anonymous"></script> --}}
		<link href="/css/all.css" rel="stylesheet">
	</head>
	<body>
		<div id="app" class="d-flex flex-column justify-content-between">
			<div class="d-flex flex-column justify-content-between">
				<header class="navbar-header" id="header" style="position: fixed; top: 0; z-index: 2; width: 100%;">
					@include('partials.nav')
				</header>
				<main id="main" class="align-top p-2 h-screen" style="height: 100%; position: absolute; bottom: 0; width: 100%; background-color: white;">
					<div style="height: 115px;">

					</div>
					@yield('content')
					<div style="height: 50px;">

					</div>
				</main>
				<footer id="footer" style="position: fixed; bottom: 0; z-index: 2; width: 100%;" class="bg-primary text-black-50 text-center py-3 shadow">
					{{ config('app.name')}} | Copyright @ {{ date('Y') }}
				</footer>
			</div>
		</div>
	</body>
</html>
