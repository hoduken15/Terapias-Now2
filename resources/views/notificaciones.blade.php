@extends('layout')

@section('title', 'Notificaciones')

@section('content')

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="box-header with-border container">
				<h3 class="box-title">Notificaciones</h3>
			</div>
		</div>
		@if(isset($mensaje))
			<div class="container alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
			</div>
		@endif
		@if (!$notificaciones->isEmpty())
			@foreach($notificaciones as $notificacion)
				<div class="alert {{ $notificacion->estado == 'No leido' ? 'alert-info' : 'bg-light border' }} alert-dismissible">
		            <a type="button" href="{{ route('notificaciones.dismiss',$notificacion) }}" class="close" aria-hidden="true">&times;</a>
		            <a href="#" data-toggle="modal" data-target="#modal-{{ $notificacion->id }}">
		            	<h4 class="text-dark"><i class="icon fa fa-info"></i> {{ $notificacion->titulo }} </h4>
		            	<label class="text-dark">{{ $notificacion->extracto }}</label>
		            </a>
		            <small><p style="color: grey;">{{ $notificacion->created_at }}</p></small>
		        </div>
		        
		        <div class="modal fade" id="modal-{{ $notificacion->id }}">
		        	<div class="modal-dialog">
		        		<div class="modal-content">
		        			<div class="modal-header">
		        				<a href="#" type="button" style="padding: 1rem 1rem;" data-dismiss="modal" aria-label="Close">
		        					<span aria-hidden="true">&times;</span>
		        				</a>
		        				<h2 class="modal-title col" >{{ $notificacion->titulo }}</h2>
		        			</div>
		        			<div class="modal-body">
		        				<p>{{ $notificacion->descripcion }}</p>
		        			</div>
		        			<div class="modal-footer">
		        				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
		        			</div>
		        		</div>
		        	</div>
		        </div>
			@endforeach
		@else
			<div class="card text-center">
		      	<div class="card-body">
		        	<h5 class="card-title">No tienes notifiaciones nuevas.</h5>
		      	</div>
	    	</div>
		@endif
	</div>
</div>

@endsection