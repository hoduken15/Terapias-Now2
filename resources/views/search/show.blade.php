@extends('layout')

@section('title', $terapia->nombre)

@section('content')
<div class="container">
	<div class="container">
		<div class="d-flex row justify-content-center">
			<div class="col-12 d-flex">
				<div class="card" style="width: 18rem;">
					@if(isset($terapia->imagen))
						<img src="/imagenes/terapias/{{$terapia->imagen}}" alt="Terapia-{{$terapia->id}}" class="card-img-top rounded img-fluid"/>
					@else
						<img src="/imagenes/terapias/default.jpg" alt="Terapia-{{$terapia->id}}" class="card-img-top rounded img-fluid"/>
					@endif
					<div class="card-body text-center">
						<h5 class="card-title">{{ $terapia->nombre }}</h5>
						<p class="card-text">{{ $terapia->descripcion }}</p>
						<a href="javascript:history.back()" class="btn btn-primary">Volver</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection