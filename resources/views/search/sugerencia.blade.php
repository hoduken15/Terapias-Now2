@extends('layout')

@section('title', 'Sugerencia de Terapias')

@section('content')

<div class="container">
	<div class="box box-primary">
		<div class="row">
			<div class="box-header with-border container">
				<h3 class="box-title">Sugerencia de Terapias</h3>
			</div>
		</div>
		@if(isset($mensaje))
			<div class="container alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-check"></i> {{ $mensaje }} </h4>
			</div>
		@endif
		<form role="form" id="formulario" method="POST" action="{{ route('search.sugerencia') }}">
			{{ csrf_field() }}
			<div class="box-body">
				<div class="form-group">
					<label for="select">Busca tus sintomas</label>
					<small><small><label for="select">Selecciona 5!</label></small></small>
					<div class="row">
						<div class="col">
							<select id="select" class="form-control">
								@foreach($palabras as $palabra)
									<option value="{{ $palabra->id }}">{{ $palabra->palabra }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div id="alerta-maximo" style="display: none;" class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" aria-hidden="true" onclick="cierraAlerta('alerta-maximo');">&times;</button>
		                <h4><i class="icon fa fa-ban"> </i><label>Solo se pueden seleccionar 5.</label></h4>
	             	</div>
	             	<div id="alerta-existe" style="display: none;" class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" aria-hidden="true" onclick="cierraAlerta('alerta-existe');">&times;</button>
		                <h4><i class="icon fa fa-ban"> </i><label>Ya se selecciono ese sintoma.</label></h4>
	             	</div>
				</div>
			</div>
			<ul id="theList" class="d-flex justify-content-start row" style="list-style-type: none;">
			</ul>
			<dir id="theCheck"></dir>
			<div class="box-footer p-2">
				<a href="#" onclick="agregaItem();" type="button" class="btn btn-success">Añadir</a>
				<a href="#" onclick="searchTerapia();" type="button" class="btn btn-primary">Buscar</a>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	var arraylistpalabras = [];
	var arraylistid = [];

	function agregaItem() {
		var palabra = document.getElementById('select').selectedOptions;
		var id = document.getElementById('select').value;
		if (arraylistpalabras.length < 5) {
			if (arraylistpalabras.includes(palabra.item(0).text)) {
				document.getElementById('theList').innerHTML = "";
				for (var i = 0; i < arraylistpalabras.length; i++) {
					document.getElementById('theList').innerHTML += ('<li class="tagitem col-sm-6 col-6 col-md-4 col-lg-2 col-xl-2" value="'+arraylistid[i]+'">'+arraylistpalabras[i]+'</li>');
				}
				document.getElementById('alerta-existe').style.display = 'block';
			} else {
				arraylistpalabras.push(palabra.item(0).text);
				arraylistid.push(id);
				document.getElementById('theList').innerHTML = "";
				for (var i = 0; i < arraylistpalabras.length; i++) {
					document.getElementById('theList').innerHTML += ('<li class="tagitem col-sm-6 col-6 col-md-4 col-lg-2 col-xl-2" value="'+arraylistid[i]+'">'+arraylistpalabras[i]+'</li>');
				}
				document.getElementById('alerta-maximo').style.display = 'none';
				document.getElementById('alerta-existe').style.display = 'none';
			}
		} else {
			document.getElementById('alerta-maximo').style.display = 'block';
		}
	}

	function cierraAlerta(id) {
		document.getElementById(id).style.display = 'none';
	}

	function searchTerapia() {
		if (arraylistpalabras.length == 5) {
			for (var i = arraylistid.length - 1; i >= 0; i--) {
				console.log(arraylistid[i]);
				document.getElementById('theCheck').innerHTML += ('<input type="checkbox" style="display: none;" checked name="palabras[]" value="'+arraylistid[i]+'">');
			}
			document.getElementById("formulario").submit();
		} else {
			alert("Selecciona 5 sintomas!");
		}
	}
</script>

@endsection