@extends('layout')

@section('title', 'Busqueda')

@section('content')

<div class="container-fluid p-3">
	@if($busquedaTerapia)
		@foreach($busquedaTerapia as $terapia)
			<div class="page-header text-center">
		    	<h1><a href="{{ route('search.showterapia',$terapia)}}">{{ $terapia->nombre }}</a></h1>
		  	</div>
		  	<hr>
		  	@if(count($terapia->Terapias) > 0)
			  	@foreach($terapia->Terapias as $terapeuta)
				  	<div class="row justify-content-center">
					  	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
							<a href="{{ route('search.showterapeuta', $terapeuta)}}">
								<div class="row border bg-secondary rounded" style="margin-left: 15px; margin-right: 15px; margin-bottom: 10px;">
									{{-- <div class="col-12"> --}}
										{{-- <div class="row"> --}}
											<div class="col-3">
												@if(isset($terapeuta->imagen))
												    <img src="/imagenes/usuarios/{{$terapeuta->imagen}}" alt="Usuario-{{$terapeuta->id}}" class="img-fluid rounded" />
												@else
												    <img src="/imagenes/usuarios/default-user.jpg" alt="Usuario-{{$terapeuta->id}}" class="img-fluid rounded"/>
												@endif
											</div>
											<div class="col-6">
												<h5 class="text-primary">{{ $terapeuta->name }}</h5>
												@if(isset($terapeuta->Centros))
													<small><p style="color: texto;">{{ $terapeuta->Centros->nombre }}</p></small>
												@endif
												<p>{{ $terapeuta->descripcion }}</p>
											</div>
											<div class="col-3">
												@foreach($terapeuta->TerapiasUser as $precio)
													@if($precio->terapias_id == $terapia->id)
														<small>Precio ${{ $precio->precio }}.-</small>
													@endif
												@endforeach
											</div>
										{{-- </div> --}}
									{{-- </div> --}}
								</div>
							</a>
						</div>
					</div>
				@endforeach 
			@else
				<div class="card text-center">
			      	<div class="card-body">
			        	<h5 class="card-title">No se encontraron Terapeutas para la terapia consultada.</h5>
			      	</div>
		    	</div>
			@endif
		@endforeach 
	@elseif($terapeutas)
		@foreach($terapeutas as $terapeuta)
			<a href="{{ route('search.showterapeuta', $terapeuta)}}">
			    <div class="card text-center">
			      	<div class="card-body">
			        	<h5 class="card-title">{{ $terapeuta->name }}</h5>
			        	<p class="card-text">{{ $terapeuta->email }}</p>
			      	</div>
			    </div>
			</a>
		@endforeach 
	@else
		<div class="card text-center">
	      	<div class="card-body">
	        	<h5 class="card-title">No se encontraron resultados.</h5>
	      	</div>
    	</div>
	@endif
</div>

@endsection