import routes from "./routes";

require('./bootstrap');

window.Vue = require('vue');
import router from "./routes";
import inputbusqueda from "./views/inputbusqueda";

const app = new Vue({
    components:{ inputbusqueda},
    el: '#app',
    router,
    data: {
        opcion: '',
    }
});
