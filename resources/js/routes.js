import Vue from 'vue'
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            component: require('./views/Home').default,
            name: 'home'
        },
        {
            path: '/home',
            redirect: {name: 'home'}
        },
        {
            path: '/home/:id',
            component: require('./views/noticias/Show').default,
            name: 'home-noticias'
        },
        {
            path: '/notificaciones',
            component: require('./views/Notificaciones').default,
            name: 'notificaciones'
        },
        {
            path: '/notificaciones/:id/dismiss',
            component: require('./views/Notificaciones').default,
            name: 'notificaciones-dismiss'
        },
        {
            path: '/terapias',
            component: require('./views/Terapias').default,
            name: 'terapias',
        },
        {
            path: '/perfil',
            component: require('./views/Perfil').default,
            props: true,
            name: 'perfil',
            children: [
                {
                    path: 'noticias',
                    component: require('./views/perfiles/admin/MenuNoticias').default,
                    name: 'noticias-index'
                },
                {
                    path: 'noticias/crear',
                    name: 'noticias-crear',
                    component: require('./views/perfiles/admin/noticias/Crear').default
                },
                {
                    path: 'admin/terapias',
                    name: 'admin-terapias',
                    component: require('./views/perfiles/admin/MenuTerapias').default,
                },
                {
                    path: 'admin/palabras/terapias',
                    name: 'sintomas-index',
                    component: require('./views/perfiles/admin/palabrasClave/Asignar').default,
                },
                {
                    path: 'admin/palabras/terapias/add',
                    name: 'admin-palabras',
                    component: require('./views/perfiles/admin/palabrasClave/Asignar').default,
                },
                {
                    path: 'admin/terapia/asigna/:id',
                    name: 'admin-asigna',
                    component: require('./views/perfiles/admin/MenuTerapias').default,
                },
                {
                    path: 'admin/noticias/listar',
                    name: 'noticias-listar',
                    component: require('./views/perfiles/admin/noticias/Lista').default
                },
                {
                    path: 'noticias/:noticia',
                    name: 'noticias-show',
                    component: require('./views/perfiles/admin/noticias/Show').default
                },
                {
                    path: 'noticias/crear',
                    component: require('./views/perfiles/admin/noticias/Crear').default
                },
                {
                    path: 'noticias/:noticia/editar',
                    name: 'noticias-edit',
                    component: require('./views/perfiles/admin/noticias/Edit').default,
                },
                {
                    path: 'terapeuta/terapias/:id',
                    component: require('./views/perfiles/terapeuta/Terapias').default,
                    name: 'terapias-terapeuta',
                    props: true
                },
                {
                    path: 'terapeuta/comentarios',
                    component: require('./views/perfiles/terapeuta/Comentarios').default
                },
                {
                    path: 'terapeuta/:id/horarios',
                    component: require('./views/schedule/Show').default,
                    name: 'terapeuta-horarios',
                },
                {
                    path: 'admin/terapeutas',
                    component: require('./views/perfiles/admin/Terapeutas').default
                },
                {
                    path: 'admin/usuarios',
                    component: require('./views/perfiles/admin/Usuarios').default,
                    name: 'admin-usuarios'
                },
                {
                    path: 'admin/terapias',
                    component: require('./views/perfiles/admin/Terapias').default
                },
                {
                    path: 'admin/noticias',
                    component: require('./views/perfiles/admin/Noticias').default
                },
                {
                    path: 'usuario/horas',
                    component: require('./views/perfiles/usuario/Horas').default
                },
                {
                    path: 'usuario/comentarios',
                    component: require('./views/perfiles/usuario/Comentarios').default
                },
                {
                    path: 'usuario/actualizar',
                    component: require('./views/perfiles/usuario/Actualizar').default
                },
                {
                    path: 'admin/terapeutas/edit/:id',
                    name: 'users-adminedit',
                    component: require('./views/perfiles/admin/terapeutas/Update').default
                },
                {
                    path: 'admin/terapias/listar',
                    name: 'terapias-listar',
                    component: require('./views/perfiles/admin/terapias/List').default
                },
                {
                    path: 'palabras',
                    name: 'palabras-index',
                    component: require('./views/perfiles/admin/palabrasClave/Crear').default
                },
                {
                    path: 'palabras/eliminar',
                    name: 'palabras-eliminar',
                    component: require('./views/perfiles/admin/palabrasClave/Crear').default
                },
                {
                    path: 'solicitudes',
                    name: 'solicitudes-index',
                    component: require('./views/perfiles/admin/solicitudes/Lista').default
                },
                {
                    path: 'user',
                    name: 'user-index',
                    component: require('./views/perfiles/admin/terapeutas/Lista').default
                },
                {
                    path: 'admin/terapeutas/:id',
                    name: 'users-verterapeuta',
                    component: require('./views/perfiles/admin/terapeutas/Show').default
                },
                {
                    path: 'admin/terapia/asignar/:id',
                    name: 'users-verasigna',
                    component: require('./views/perfiles/admin/terapias/Asigna').default
                },
                {
                    path: 'terapias/crear',
                    name: 'terapias-create',
                    component: require('./views/perfiles/admin/terapias/Crear').default
                },
                {
                    path: 'admin/terapias/:id',
                    name: 'terapias-muestra',
                    component: require('./views/perfiles/admin/terapias/Show').default
                },
                {
                    path: 'terapias/:terapia/editar',
                    name: 'terapias-edit',
                    component: require('./views/perfiles/admin/terapias/Update').default
                },
                {
                    path: 'admin/user/:id',
                    name: 'admin-user-show',
                    component: require('./views/perfiles/admin/UserShow').default
                },
                {
                    path: 'atenciones/ver/:user',
                    name: 'atenciones-ver',
                    component: require('./views/perfiles/terapeuta/Atenciones').default
                },
                {
                    path: ':id/comentarios',
                    name: 'perfil-comentarios',
                    component: require('./views/perfiles/terapeuta/Comentarios').default
                },
                {
                    path: 'comentarios/terapeuta/crea/:id',
                    name: 'comentarios-comentaterapeuta',
                    component: require('./views/perfiles/terapeuta/Evaluar').default
                },
                {
                    path: 'terapias/anadir/:user',
                    name: 'terapias-anadir',
                    component: require('./views/perfiles/terapeuta/SolicitarTerapia').default
                },
                {
                    path: 'terapias/:terapia',
                    name: 'terapias-show',
                    component: require('./views/perfiles/terapeuta/Terapias').default
                },
                {
                    path: 'perfil/:id/agendamiento/:terapia',
                    name: 'perfil-hora',
                    component: require('./views/perfiles/terapeuta/TomaHora').default
                },
                {
                    path: 'user/terapia/:id',
                    name: 'terapia-userupdate',
                    component: require('./views/perfiles/terapeuta/VerTerapia').default
                },
                {
                    path: 'perfil/:id/:terapia',
                    name: 'perfil-miterapia',
                    component: require('./views/perfiles/terapeuta/VerTerapia').default
                },
                {
                    path: 'atenciones/:atenciones',
                    name: 'atenciones-show',
                    component: require('./views/perfiles/usuario/Atenciones').default
                },
                {
                    path: 'comentarios/user/crea/:id',
                    name: 'comentarios-comentar',
                    component: require('./views/perfiles/usuario/Evaluar').default
                },
                {
                    path: 'comentarios/user/:id',
                    name: 'comentarios-showuser',
                    component: require('./views/perfiles/usuario/Comentarios').default
                },
                {
                    path: 'comentarios/:id',
                    name: 'comentarios-showterapeuta',
                    component: require('./views/perfiles/usuario/Evaluaciones').default
                },
                {
                    path: 'user/descripcion/:id',
                    name: 'user-descripcion',
                    component: require('./views/perfiles/usuario/Descripcion').default
                },
                {
                    path: 'user/:id/formulario',
                    name: 'user-formluario',
                    component: require('./views/perfiles/usuario/Solicita').default
                },
                {
                    path: 'user/:user/editar',
                    name: 'user-edit',
                    component: require('./views/perfiles/usuario/Actualizar').default
                },
            ],
        },
        {
            path: '/search/:data',
            name: 'search',
            component: require('./views/Search').default
        },
        {
            path: '/search/sugerencia',
            name: 'search-sugerencia',
            component: require('./views/Search').default
        },
        {
            path: '/search/terapia/:id/show',
            name: 'search-showterapia',
            component: require('./views/search/Show').default
        },
        {
            path: '/SugerenciaTerapias',
            name: 'sugerenciaTerapias',
            component: require('./views/search/Sugerencia').default
        },
        {
            path: '/search/terapeuta/:id/show',
            name: 'search-showterapeuta',
            component: require('./views/Perfil').default
        },
        {
            path: '/perfil/:id/horario',
            name: 'perfil-horario',
            component: require('./views/schedule/Show').default
        },
        {
            path: '/schedule',
            name: 'schedule-index',
            component: require('./views/schedule/Index').default
        },
        {
            path: '/schedule/crear',
            name: 'schedule-create',
            component: require('./views/schedule/Create').default
        },
        {
            path: '/schedule/:schedule',
            name: 'schedule-update',
            component: require('./views/schedule/Update').default
        },
    ],
})
