<?php

use Illuminate\Database\Seeder;

class NoticiasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=0; $i<=25; $i++):
            DB::table('noticias')
                ->insert([
                    'titulo' => $faker->word,
                    'detalle' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                    'extracto' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                    'fecha' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),
                ]);
        endfor;
    }
}
