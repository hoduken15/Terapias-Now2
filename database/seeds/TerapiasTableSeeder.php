<?php

use App\Terapias;
use Illuminate\Database\Seeder;

class TerapiasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terapias')->insert([
        	['nombre' => 'Cirugia Astral', 'descripcion' => 'Para comprender la "Cirugía Astral" es necesario partir del concepto de que el ser humano es mucho más que un cuerpo físico. Eres la suma de tu cuerpo, de tus pensamientos, de tus emociones y de tu alma.
				La cirugía Astral consiste en limpiar el campo aurico de todas las estructuras, parásitos, larvas, alquitrán etc, que obstaculiza el flujo energético ya sea de una persona  o de un lugar.'],
        	['nombre' => 'Liberación de Emociones', 'descripcion' => 'La mayoría de las enfermedades empiezan cuando nuestra energía no vibra correctamente y no fluye expandida por nuestro cuerpo. Nuestros pensamientos, emociones y programaciones negativas adoptan una forma densa, a modo de patrones de energía cristalizados en nuestros campos etéricos. Esos patrones cristalizados van penetrando gradualmente, hasta que, en última instancia, se manifiestan como la enfermedad física en el cuerpo, nuestro campo electromagnético más denso.'],
        	['nombre' => 'Canalización', 'descripcion' => 'La canalización nos ayuda a establecer una comunicación desde la paz y el amor incondicional para cerrar ciclos con seres queridos fallecidos. Si quedaron temas pendientes de comunicar o resolver  la canalización nos permite ser una canal de comunicación para restaurar la paz en nuestras relaciones o simplemente recordar que los lazos de amor son eternos y que el vínculo solamente se transforma.'],
        	['nombre' => 'Registros Akashicos', 'descripcion' => 'La canalización energética es el proceso mediante el cual una persona se dispone como un canal consciente para recibir mensajes de energías y seres de luz. A través de ella, podemos acceder a infinitas fuentes de información, hacer diagnósticos certeros acerca de padecimientos propios o de otros, comprender las causas de ciertos síntomas o problemáticas, y obtener respuestas y soluciones a temáticas de importancia vital. Podemos conocer aspectos del plan de nuestra alma, del sentido de nuestra vida y de nuestras misiones en esta encarnación, generar procesos de autoconocimiento y de sanación personal y de otros.'],
        	['nombre' => 'Tarot', 'descripcion' => 'El tarot es el depositario de toda la evolución del cosmos, de la tierra y del ser humano, que a través de números y símbolos intenta hacernos llegar este conocimiento para que conecte con la sabiduría que desde tiempos remotos hemos adquirido.
				Con lectura de tarot podrás encontrar guía y sanación para eventos actuales o pasados que te aquejen, además de acceder a los futuros posibles de acuerdo a tus dudas.'],
        	['nombre' => 'Regresiones Terapeuticas', 'descripcion' => 'Nuestros recuerdos y vivencias permanecen con nosotros a lo largo del tiempo. Por eso es importante trabajarlos, especialmente cuando nos condiciona o tiene un origen no deseable como puede ser el dolor ocasionado por una situación traumática del pasado, que está condicionando la forma que percibimos nuestra vida actual. La terapia regresiva a vidas pasadas o hipnosis regresiva permite liberar y sanar ese dolor, esas energías que nos desequilibran.
				La terapia regresiva consiste en una relajación muy profunda para conectar con la información que llevamos almacenada en nuestro interior ya la cual no podemos acceder desde un estado de vigilia o consciente.'],
        	['nombre' => 'Sanación del Alma', 'descripcion' => 'La mayoría de las enfermedades empiezan en uno de los cuerpos sutiles. Nuestros pensamientos, emociones y programaciones negativas adoptan una forma densa, a modo de patrones de energía cristalizados en nuestros campos etéricos. Esos patrones cristalizados van penetrando gradualmente, hasta que, en última instancia, se manifiestan como la enfermedad física en el cuerpo, nuestro campo electromagnético más denso. 
				La terapia de Sanación del Alma es la más completa al momento de realizar sanación, pues vamos al origen del problema, entendiendo que todo lo que nos enferma tiene una cura que está en el alma.
				No solo es llegar a la situación, o a la emoción, o al patrón de conducta, o perdonar o solo convencernos de que debemos amarnos, llegar a la herida de origen es el objetivo de la sanación del alma, pues así y solo así, liberaremos nuestra alma en forma definitiva y podremos comenzar a reescribir nuestra historia.'],
        	['nombre' => 'Constelaciones Familiares', 'descripcion' => 'Con las Constelaciones Familiares se busca que los integrantes de la familia reconozcan que cada miembro lo hizo como pudo y eso ya está hecho. Cuando se toma conciencia de esta situación, entonces se podrá actuar con vida y sentimientos propios. A través de las Constelaciones Familiares comenzamos a tener una nueva imagen de nuestro sistema familiar y sobre todo a sentir un profundo agradecimiento con nuestros antepasados
				Es una técnica que lleva al individuo a observar el lugar que ocupa al interior de su familia, para así solucionar conflictos que, incluso, pueden traspasarse de generación en generación'],
        	['nombre' => 'Auriculoterapia', 'descripcion' => 'Terapia basada en la medicina China, se realiza en  el pabellón auricular el cual  refleja un mapa de nuestro organismo o Somato topia. Se utilizan semillas de Vaccaria o Imanes, los cuales van estimulando los diferentes puntos de la Oreja para la liberación de hormonas y estimulación de los sistemas nerviosos en el cuerpo.
				Este tratamiento es efectivo en cualquier tipo de patología, estrés, ansiedad, bajar de peso, dolores musculares, migrañas, asma, alergias, insomnio, entre otras.'],
        	['nombre' => 'Gemoterapia', 'descripcion' => 'Las gemas y cristales tienen la capacidad de transmitirnos energía, pudiendo aumentar y concentrar nuestra energía curativa, restableciendo el equilibrio en nuestro cuerpo a nivel físico, emocional, mental y espiritual.
				Los principales beneficios son:
				- Revitaliza y reduce el estrés
				- Estimula el sistema inmunológico
				- Equilibra los chakras
				- Mejora el estado del ánimo
				- Incrementa el autoestima
				- Da equilibrio emocional y claridad mental
				- Alivia dolores y estimula la curación'],
        	['nombre' => 'Sanación Egipcia con Cruz de Ankh', 'descripcion' => 'La cruz de Ankh es una herramienta de sanación utilizada por los Egipcios ya que tiene la capacidad de emitir radiaciones sutiles benéficas. Sana las energías del cuerpo, depura chakras, armoniza energías, a nivel mental, emocional, y espiritual.
				Esta Sanación conecta con el conocimiento universal para que el paciente desde ese momento comience a realizar su aprendizaje desde la energía del amor y dejar de aprender desde el sufrimiento.'],
        	['nombre' => 'Reiki', 'descripcion' => 'El reiki es la energía universal de amor, al ser entregada, libera toda energía negativa, sea creada por la persona o por externo, activa los puntos energéticos, los limpia y te conecta con la energía universal.
				Al comenzar a fluir en la energía del universo, todo lo negativo en tu vida se va, ves con claridad tu camino y sientes los deseos de comenzar a recorrerlo. El universo vibra en amor y el reiki universal te conecta con el ser.']
    	]);
    }
}
