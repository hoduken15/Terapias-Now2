<?php

use Illuminate\Database\Seeder;

class CentrosTerapeuticos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('centrosterapeuticos')->insert([
            ['nombre' => 'Centro Terapeutico Kosyka', 'direccion' => 'El Coiron 10654, La Florida'],
        ]);
    }
}
