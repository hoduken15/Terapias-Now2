<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=0; $i<=15; $i++):
            DB::table('Users')
                ->insert([
                    'name' => $faker->name,
                    'rut' => Rut::set(rand(18000000, 25000000))->fix()->format(),
                    'fecha_nacimiento' => $faker->dateTimeBetween($startDate = '-45 years', $endDate = '-18 years', $timezone = null),
                    'comuna_id' => 1,
                    'direccion' => $faker->address,
                    'phone' => $faker->phoneNumber,
                    'email' => $faker->email,
                    'descripcion' => $faker->text($maxNbChars = 200),
                    'password' => Hash::make(123123123), // password
        			'remember_token' => Str::random(10),
                ]);
        endfor;

        // DB::table('Users')
        //     ->insert([
        //         'name' => 'Javier López',
        //         'email' => 'javier_lopez@ciisa.cl',
        //         'password' => Hash::make(123123123), // password
        //         'remember_token' => Str::random(10),
        //         'rut' => '19.573.152-7',
        //         'fecha_nacimiento' => "1997-03-04",
        //         'comuna_id' => 26,
        //         'direccion' => 'El Coiron 10654',
        //         'phone' => '944966895',
        //         'descripcion' => null,
        //     ]);

        // DB::table('Users')
        //     ->insert([
        //         'name' => 'Administrador',
        //         'email' => 'admin@ciisa.cl',
        //         'password' => Hash::make(123123123), // password
        //         'remember_token' => Str::random(10),
        //         'rut' => '11.111.111-1',
        //         'fecha_nacimiento' => "2000-01-01",
        //         'comuna_id' => 26,
        //         'direccion' => 'Calle falsa 123',
        //         'phone' => '912341234',
        //         'descripcion' => null,
        //     ]);

        // DB::table('Users')
        //     ->insert([
        //         'name' => 'Terapeuta',
        //         'email' => 'terapeuta@ciisa.cl',
        //         'password' => Hash::make(123123123), // password
        //         'remember_token' => Str::random(10),
        //         'rut' => '11.111.111-2',
        //         'fecha_nacimiento' => "2000-01-01",
        //         'comuna_id' => 32,
        //         'direccion' => 'Calle falsa 456',
        //         'phone' => '943214321',
        //         'descripcion' => null,
        //     ]);

        // DB::table('Users')
        //     ->insert([
        //         'name' => 'Usuario',
        //         'email' => 'user@ciisa.cl',
        //         'password' => Hash::make(123123123), // password
        //         'remember_token' => Str::random(10),
        //         'rut' => '11.111.111-3',
        //         'fecha_nacimiento' => "2000-01-01",
        //         'comuna_id' => 34,
        //         'direccion' => 'Calle el robo',
        //         'phone' => '9123456789',
        //         'descripcion' => null,
        //     ]);
    }
}
