<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitudes extends Model
{
    protected $fillable = [
        'user_id', 'terapia_id', 'descripcion', 'precio', 'estado'
    ];

    public function Usuarios()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function Terapias()
    {
        return $this->belongsTo('App\Terapias','terapia_id','id');
    }

}
