<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Palabras extends Model
{
    protected $table = 'palabras_clave';

    public function Terapias()
    {
        return $this->belongsToMany('App\Terapias');
    }

}
