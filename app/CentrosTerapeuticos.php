<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentrosTerapeuticos extends Model
{
    protected $table = 'centrosterapeuticos';
}
