<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rut', 'fecha_nacimiento', 'centro_id', 'phone'
    ];

//     protected $attributes = [
//         'id_perfil' => '1',
//     ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function User()
    {
        return $this->belongsToMany('App\Terapias')->withPivot('precio');
    }

    public function Horas()
    {
        return $this->hasMany('App\Horarios');
    }

    public function TerapiasUser()
    {
        return $this->hasMany('App\TerapiasUser');
    }

    public function TerapiasPrecio($id)
    {
        return $this->hasMany('App\TerapiasUser', 'terapias_id', $id);
    }

    public function PuntajeTerapeuta()
    {
        return $this->hasMany('App\Comentarios','id_terapeuta','id');
    }

    public function PuntajeUsuario()
    {
        return $this->hasMany('App\Comentarios','id_cliente','id');
    }

    public function Centros()
    {
        return $this->belongsTo('App\CentrosTerapeuticos','centro_id','id');
    }

    public function scopeName($query, $name)
    {
        if ($name) {
            return $query->where('name','LIKE',"%$name%");
        }
    }
    public function Terapias()
    {
        return $this->belongsToMany('App\Terapias')->withPivot('precio');
    }

}
