<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentarios extends Model
{
    public function ComentariosTerapeutas()
    {
        return $this->belongsTo('App\User','id_terapeuta','id');
    }

    public function ComentariosUsuario()
    {
        return $this->belongsTo('App\User','id_cliente','id');
    }

    public function ComentarioAtencion()
    {
    	return $this->belongsTo('App\Atenciones','id_atencion','id');
    }
}
