<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerapiasUser extends Model
{
    protected $table = 'terapias_user';
}
