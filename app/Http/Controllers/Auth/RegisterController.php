<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function messages()
    {
        return [
            'email' => [
                'required' => 'El email es obligatorio.',
            ],
            'nombre' => [
                'required' => 'El nombre es obligatorio.',
                'max' =>'El nombre no puede ser mayor a :max caracteres.',
            ],
            'rut' => [
                'required' => 'El Rut es obligatorio.',
                'unique' => 'El rut indicado ya se encuentra registrado.',
            ],
            'password' => [
                'required' => 'La contraseña es obligatorio.',
                'min' => 'La contraseña debe tener un minimo de :min caracteres.',
                'confirmed' => 'Confirma la contraseña.'
            ]
        ];
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => ['required', 'string', 'max:255'],
            'rut' => ['required', 'unique:users'],
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['nombre'],
            'rut' => $data['rut'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }
}
