<?php

namespace App\Http\Controllers;

use App\Palabras;
use App\PalabrasTerapias;
use App\Terapias;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        if ( $request->get('buscar')!="" ) {
            $busquedaTerapia = Terapias::Name($request->get('buscar'))->with(['Terapias','Terapias.Centros'])->get();

            if ((bool) count($busquedaTerapia)) {

                $array['busquedaTerapia'] = $busquedaTerapia;
                $array['user'] = $user;

                if (request()->wantsJson() ) {
                    return $array;
                }

                return view('search', [
                    'busquedaTerapia' => $busquedaTerapia,
                    'user' => $user
                ]);
            } else {
                if ($user->id_perfil != 3) {
                    $terapeutas = User::Name($request->get('buscar'))->where('id_perfil','2')->with('Centros')->get();
                    if ((bool) count($terapeutas)) {

                        $array['terapeutas'] = $terapeutas;
                        $array['busquedaTerapia'] = null;
                        $array['user'] = $user;

                        if (request()->wantsJson() ) {
                            return $array;
                        }

                        return view('search', [
                            'terapeutas' => $terapeutas,
                            'busquedaTerapia' => null,
                            'user' => $user
                        ]);
                    } else {

                        $array['terapeutas'] = null;
                        $array['busquedaTerapia'] = null;
                        $array['user'] = $user;

                        if (request()->wantsJson() ) {
                            return $array;
                        }

                        return view('search', [
                            'terapeutas' => null,
                            'busquedaTerapia' => null,
                            'user' => $user
                        ]);
                    }
                } else {
                    $terapeutas = User::Name($request->get('buscar'))->with('Centros')->get();
                    if ((bool) count($terapeutas)) {

                        $array['terapeutas'] = $terapeutas;
                        $array['busquedaTerapia'] = null;
                        $array['user'] = $user;

                        if (request()->wantsJson() ) {
                            return $array;
                        }

                        return view('search', [
                            'terapeutas' => $terapeutas,
                            'busquedaTerapia' => null,
                            'user' => $user
                        ]);
                    } else {

                        $array['terapeutas'] = null;
                        $array['busquedaTerapia'] = null;
                        $array['user'] = $user;

                        if (request()->wantsJson() ) {
                            return $array;
                        }

                        return view('search', [
                            'terapeutas' => null,
                            'busquedaTerapia' => null,
                            'user' => $user
                        ]);
                    }
                }
                $terapeutas = User::Name($request->get('buscar'))->with('Centros')->get();
                if ((bool) count($terapeutas)) {

                    $array['terapeutas'] = $terapeutas;
                    $array['busquedaTerapia'] = null;
                    $array['user'] = $user;

                    if (request()->wantsJson() ) {
                        return $array;
                    }

                    return view('search', [
                        'terapeutas' => $terapeutas,
                        'busquedaTerapia' => null,
                        'user' => $user
                    ]);
                } else {

                    $array['terapeutas'] = null;
                    $array['busquedaTerapia'] = null;
                    $array['user'] = $user;

                    if (request()->wantsJson() ) {
                        return $array;
                    }

                    return view('search', [
                        'terapeutas' => null,
                        'busquedaTerapia' => null,
                        'user' => $user
                    ]);
                }
            }

        } else {
            $array['terapeutas'] = null;
            $array['busquedaTerapia'] = null;
            $array['user'] = $user;

            if (request()->wantsJson() ) {
                return $array;
            }

            return view('search', [
                'terapeutas' => null,
                'busquedaTerapia' => null,
                'user' => $user
            ]);
        }
    }

    public function showterapia($id)
    {
        $terapia = Terapias::find($id);
        $user = auth()->user();

        $array['user'] = $user;
        $array['terapia'] = $terapia;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('search.show',[
            'terapia' => $terapia,
            'user' => $user,
        ]);
    }

    public function showterapeuta($id)
    {
        $user = User::find($id);

        $array['user'] = $user;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('search.show',[
            'user' => User::find($id),
        ]);
    }

    public function sugerencia(Request $request)
    {
        $palabras = $request->palabras;
        $terapias = [];
        $lista = [];
        foreach ($palabras as $key => $palabra) {
            $ter = PalabrasTerapias::where('palabras_id', $palabra)->get();
            $terapias[$key] = $ter[0];
        }
        foreach ($terapias as $key => $value) {
            $lista[] = $value->terapias_id;
        }
        $conteo = collect($lista)->countBy();
        foreach ($conteo as $key => $value) {
            if ($conteo[$key] == $conteo->max()) {
                $terapia[] = Terapias::find($key);
            }
        }

        $user = auth()->user();

        $array['terapeutas'] = null;
        $array['busquedaTerapia'] = $terapia;
        $array['user'] = $user;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('search', [
            'terapeutas' => null,
            'busquedaTerapia' => $terapia,
            'user' => $user
        ]);
    }

}
