<?php

namespace App\Http\Controllers;

use App\Atenciones;
use App\Comentarios;
use App\Horarios;
use App\Services\ServicioDatos;
use App\Terapias;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ComentariosController extends Controller
{

    public $service;
    public function __construct(ServicioDatos $servicio){

        $this->middleware('auth');
        $this->service = $servicio;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comentarios = Comentarios::all();
        return $comentarios;
    }

    public function comentar($id)
    {
        $atencion = Atenciones::find($id);
        // Buscar si tiene un comentario
        $terapeuta = User::find($atencion->id_terapeuta);
        $terapia = Terapias::find($atencion->id_terapia);
        $horario = Horarios::find($atencion->id_horario);
        $cliente = User::find($atencion->id_cliente);

        $array['user'] = auth()->user();
        $array['atencion'] = $atencion;
        $array['terapeuta'] = $terapeuta;
        $array['terapia'] = $terapia;
        $array['horario'] = $horario;
        $array['cliente'] = $cliente;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.user.evaluar',[
            'user' => auth()->user(),
            'atencion' => $atencion,
            'terapeuta' => $terapeuta,
            'terapia' => $terapia,
            'horario' => $horario,
            'cliente' => $cliente,
        ]);
    }

    public function comentaterapeuta($id)
    {
        $atencion = Atenciones::find($id);
        // Buscar si tiene un comentario
        $terapeuta = User::find($atencion->id_terapeuta);
        $terapia = Terapias::find($atencion->id_terapia);
        $horario = Horarios::find($atencion->id_horario);
        $cliente = User::find($atencion->id_cliente);

        $array['user'] = auth()->user();
        $array['atencion'] = $atencion;
        $array['terapeuta'] = $terapeuta;
        $array['terapia'] = $terapia;
        $array['horario'] = $horario;
        $array['cliente'] = $cliente;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.terapeuta.evaluar',[
            'user' => auth()->user(),
            'atencion' => $atencion,
            'terapeuta' => $terapeuta,
            'terapia' => $terapia,
            'horario' => $horario,
            'cliente' => $cliente,
        ]);
    }

    public function guardar(Request $request, $id)
    {
        $atencion = Atenciones::find($id);
        $comentario = new Comentarios;
        $comentario->id_cliente = $atencion->id_cliente;
        $comentario->id_terapeuta = $atencion->id_terapeuta;
        $comentario->id_atencion = $id;
        $comentario->puntaje = $request->star;
        $comentario->comentario = $request->comentario;
        $comentario->save();

        $array['user'] = auth()->user();
        $array['mensaje'] = 'Comentario guardado exitosamente!';

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.index',[
            'user' => auth()->user(),
            'mensaje' => 'Comentario guardado exitosamente!',
        ]);
    }

    public function guardarterapeuta(Request $request, $id)
    {
        $atencion = Atenciones::find($id);
        $comentario = new Comentarios;
        $comentario->id_cliente = $atencion->id_terapeuta;
        $comentario->id_terapeuta = $atencion->id_cliente;
        $comentario->id_atencion = $id;
        $comentario->puntaje = $request->star;
        $comentario->comentario = $request->comentario;
        $comentario->save();

        $array['user'] = auth()->user();
        $array['mensaje'] = 'Comentario guardado exitosamente!';

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.index',[
            'user' => auth()->user(),
            'mensaje' => 'Comentario guardado exitosamente!',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::find($id);
        $comentarios = Comentarios::where('id_cliente',$id)->with('ComentariosUsuario')->get();

        $array['usuario'] = $usuario;
        $array['comentarios'] = $comentarios;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.terapeuta.comentarios', $this->service->datos($id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showuser($id)
    {
        $comentarios = Comentarios::where('id_cliente', $id)->with('ComentariosUsuario')->get();
        $user = User::find($id);

        $array['comentarios'] = $comentarios;
        $array['user'] = $user;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.user.comentarios', [
            'comentarios' => $comentarios,
            'user' => $user,
        ]);
    }

    public function showterapeuta($id)
    {
        $comentarios = Comentarios::where('id_terapeuta', $id)->with('ComentariosTerapeuta')->get();
        $user = User::find($id);
        
        $array['comentarios'] = $comentarios;
        $array['user'] = $user;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.user.evaluaciones', [
            'comentarios' => $comentarios,
            'user' => $user,
        ]);
    }
}
