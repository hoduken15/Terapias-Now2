<?php

namespace App\Http\Controllers;

use App\Palabras;
use Illuminate\Http\Request;

class SugerenciaTerapiasController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $palabras = Palabras::all();

        $array['user'] = $user;
        $array['palabras'] = $palabras;

        if (request()->wantsJson() ) {
            return $array;
        }

    	return view('search.sugerencia',[
    		'user' => $user,
    		'palabras' => $palabras,
    	]);
    }

    public function show()
    {
    	//
    }
}
