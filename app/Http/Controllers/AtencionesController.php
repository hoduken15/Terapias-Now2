<?php

namespace App\Http\Controllers;

use App\Atenciones;
use App\Comentarios;
use App\Horarios;
use App\Notificaciones;
use App\Services\ServicioDatos;
use App\Terapias;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AtencionesController extends Controller
{
    public $service;
    public function __construct(ServicioDatos $servicio){
        $this->middleware('auth');
        $this->service = $servicio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, $terapia)
    {
        // Atenciones de un cliente
        $user = User::find($id)->load('User.pivot');
        $terapia = Terapias::find($terapia);
        $horas = $user->Horas;
        $cliente = auth()->user();
        $hoy = Carbon::now()->setTimezone('America/Santiago')->format('Y-m-d H:m:s');
        $horarios = new Collection;
        foreach ($horas as $key => $value) {
            $fechahora = $value->fecha . " " . $value->hora;
            if ($fechahora > $hoy && $value->estado == "Disponible") {
                $horarios->push($value);
            }
        }

        $array['user'] = $user;
        $array['terapia'] = $terapia;
        $array['horas'] = $horarios;
        $array['cliente'] = $cliente;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.terapeuta.tomahora',[
            'user' => $user,
            'terapia' => $terapia,
            'horas' => $horarios,
            'cliente' => $cliente
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hora == "Selecciona una hora") {
            $user = User::find($request->terapeuta);
            $terapia = Terapias::find($request->terapia);
            $horas = $user->Horas;
            $cliente = auth()->user();

            $array['user'] = $user;
            $array['terapia'] = $terapia;
            $array['horas'] = $horas;
            $array['cliente'] = $cliente;
            $array['error'] = 'Selecciona una hora';

            if ( request()->wantsJson() ) {
                return $array;
            }

            return view('perfiles.terapeuta.tomahora',[
                'user' => $user,
                'terapia' => $terapia,
                'horas' => $horas,
                'cliente' => $cliente,
                'error' => 'Selecciona una hora'
            ]);
        }

        $atencion = New Atenciones;
        $atencion->id_cliente = auth()->user()->id;
        $atencion->id_terapeuta = $request->terapeuta;
        $atencion->id_terapia = $request->terapia;
        $atencion->id_horario = $request->hora;
        $atencion->precio = $request->precio;
        $atencion->estado = "Pendiente";
        $atencion->save();

        $horario = Horarios::find($request->hora);
        $horario->estado = "Ocupado";
        $horario->save();

        app('App\Http\Controllers\NotificacionesController')->notificaHora($request);

        $user = User::find($request->terapeuta);

        $array['user'] = $user;
        $array['mensaje'] = 'Hora solicitada con exito!';

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.index',[
            'user' => $user,
            'mensaje' => 'Hora solicitada con exito!',
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $atenciones = Atenciones::where('id_cliente',$id)->get();
        $user = User::find($id);
        $puntaje = Comentarios::where('id_terapeuta',$id)->get()->avg('puntaje');

        $array['atenciones'] = $atenciones;
        $array['user'] = $user;
        $array['puntaje'] = $puntaje;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.user.atenciones', [
            'atenciones' => $atenciones,
            'user' => $user,
            'puntaje' => $puntaje,
        ]);
    }


    public function verAtenciones($id)
    {
        $user = auth()->user();
        $atenciones = Atenciones::where('id_terapeuta',$id)->get();

        $array['user'] = $user;
        $array['atenciones'] = $atenciones;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.terapeuta.atenciones',[
            'user' => $user,
            'atenciones' => $atenciones,
        ]);
    }

}
