<?php

namespace App\Http\Controllers;

use App\Comentarios;
use App\Services\ServicioDatos;
use App\Terapias;
use App\TerapiasUser;
use App\User;
use Illuminate\Http\Request;

class TerapiaController extends Controller
{

    public $service;
    public function __construct(ServicioDatos $servicio){

        $this->middleware('auth');
        $this->service = $servicio;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comentarios = Comentarios::where('id_terapeuta', $id)->get();
        $usuario = auth()->user();

        $array['comentarios'] = $comentarios;
        $array['puntaje'] = $comentarios->avg('puntaje');
        $array['user'] = User::find($id)->load('Terapias');
        $array['usuario'] = $usuario;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.terapeuta.terapias', $this->service->datos($id));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function muestra($id)
    {
        $terapia = Terapias::find($id);
        $user = auth()->user();

        $array['user'] = $user;
        $array['terapia'] = $terapia;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapias.show', [
            'user' => $user,
            'terapia' => $terapia
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();

        $array['user'] = $user;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapias.crear_terapias',[
            'user' => $user,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->hasFile('imagen')){
            $filenameWithExt = $request->file('imagen')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $fileNameToStore = date('Y-m-d_H-i-s') . '_' . uniqid() . '_terapias' . '.' .$extension;
            request()->imagen->move(public_path('imagenes/terapias/'), $fileNameToStore);
        } else {
            $fileNameToStore = "default.png";
        }

        $terapia = New Terapias;
        $terapia->nombre = $request->nombre;
        $terapia->descripcion = $request->descripcion;
        $terapia->imagen = $fileNameToStore;
        $terapia->save();

        app('App\Http\Controllers\NotificacionesController')->notificaNuevaTerapia($request);

        $user = auth()->user();
        $mensaje = 'Terapia creada exitosamente!';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapias.crear_terapias',[
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function listar()
    {
        $terapias = Terapias::all();
        $user = auth()->user();

        $array['user'] = $user;
        $array['terapias'] = $terapias;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapias.lista',[
            'user' => auth()->user(),
            'terapias' => $terapias
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $terapia = Terapias::find($id);
        $user = auth()->user();

        $array['user'] = $user;
        $array['terapia'] = $terapia;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapias.update',[
            'terapia' => $terapia,
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $terapia = Terapias::find($id);
        $terapia->descripcion = $request->descripcion;
        if ($request->hasFile('imagen')){
            $filenameWithExt = $request->file('imagen')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $fileNameToStore = date('Y-m-d_H-i-s') . '_' . uniqid() . '_terapias' . '.' .$extension;
            request()->imagen->move(public_path('imagenes/terapias/'), $fileNameToStore);
            $terapia->imagen = $fileNameToStore;
        }
        $terapia->save();

        $user = auth()->user();
        $mensaje = 'Terapia actualizada exitosamente!';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;
        $array['terapia'] = $terapia;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapias.show',[
            'user' => $user,
            'mensaje' => $mensaje,
            'terapia' => $terapia,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $terapia = Terapias::find($id);
        $terapia->delete();

        $user = auth()->user();
        $mensaje = 'Terapia eliminada exitosamente!';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapias.lista',[
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

    public function ver($id, $terapia)
    {
        $user = User::find($id);
        $terapia = Terapias::find($terapia);

        $array['user'] = $user;
        $array['terapia'] = $terapia;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.terapeuta.verterapia',[
            'user' => $user,
            'terapia' => $terapia,
        ]);
    }

    // public function userupdate(Request $request, $id)
    // {
    //     $terapiauser = TerapiasUser::where('terapias_id',$id)->where('user_id',auth()->user()->id)->get();
    //     $terapiauser[0]->descripcion = $request->descripcion;
    //     $terapiauser[0]->save();

    //     $user = auth()->user();
    //     $terapia = Terapias::find($id);
    //     $mensaje = 'Descripcion de la terapia agregada correctamente!';

    //     $array['user'] = $user;
    //     $array['terapia'] = $terapia;
    //     $array['mensaje'] = $mensaje;

    //     if (request()->wantsJson() ) {
    //         return $array;
    //     }

    //     return view('perfiles.terapeuta.verterapia',[
    //         'user' => $user,
    //         'terapia' => $terapia,
    //         'mensaje' => $mensaje,
    //     ]);
    // }

    public function solicitarTerapia($id)
    {
        $user = User::find($id);
        if ($user->id_perfil == '2') {

            $user = auth()->user();
            $terapias = Terapias::get();

            $array['user'] = $user;
            $array['terapias'] = $terapias;

            if (request()->wantsJson() ) {
                return $array;
            }

            return view('perfiles.terapeuta.solicitaTerapia',[
                'user' => $user,
                'terapias' => $terapias,
            ]);
        } else {
            if (request()->wantsJson() ) {
                return ['user' => '',  'terapias'=>'' ];
            }
        }
    }

    public function solicitar(Request $request, $id)
    {
        app('App\Http\Controllers\NotificacionesController')->notificaSolicitudTerapia($request);
        $solicitud['user_id'] = $id;
        $solicitud['terapia_id'] = $request->terapia;
        $solicitud['descripcion'] = $request->descripcion;
        $solicitud['precio'] = $request->precio;
        $solicitud['tipo_solicitud'] = "Solicitud de Terapia";
        app('App\Http\Controllers\SolicitudesController')->store($solicitud);

        $user = auth()->user();
        $mensaje = 'Se ha realizado la solicitud de terapia!';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.index',[
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

    public function sugerir(Request $request, $id)
    {
        $terapia = New Terapias;
        $terapia->nombre = $request->nombre;
        $terapia->descripcion = $request->descripcion;
        // $terapia->imagen = $request->imagen;
        $terapia->estado = 'Pendiente';
        $terapia->save();
        $terapia = Terapias::where('nombre',$request->nombre)->get();
        app('App\Http\Controllers\NotificacionesController')->notificaSugerirTerapia($request);
        $solicitud['user_id'] = $id;
        $solicitud['terapia_id'] = $terapia[0]->id;
        $solicitud['descripcion'] = $request->descripcion;
        $solicitud['precio'] = $request->precio;
        $solicitud['tipo_solicitud'] = "Sugerencia de Terapia";
        app('App\Http\Controllers\SolicitudesController')->store($solicitud);

        $user = auth()->user();
        $mensaje = 'Se ha realizado la sugerencia y solicitud de terapia!';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.index',[
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }
}
