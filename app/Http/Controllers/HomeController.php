<?php

namespace App\Http\Controllers;

use App\Noticias;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    
    public function spa()
    {
        return view('spa.spa');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $noticias = Noticias::limit(40)->orderBy('fecha', 'desc')->get();
        $user = auth()->user();

        $array['noticias'] = $noticias;
        $array['user'] = $user;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('home',[
            'noticia' => $noticias,
            'user' => $user
        ]);
    }

    public function show($id)
    {
        $noticias = Noticias::find($id);
        $user = auth()->user();
        
        $array['noticias'] = $noticias;
        $array['user'] = $user;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('noticias.show',[
            'noticia' => $noticias,
            'user' => auth()->user()
        ]);
    }


}
