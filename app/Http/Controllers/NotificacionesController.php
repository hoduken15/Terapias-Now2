<?php

namespace App\Http\Controllers;

use App\Horarios;
use App\Notificaciones;
use App\Terapias;
use App\User;
use Illuminate\Http\Request;

class NotificacionesController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function Index()
    {
        $notificaciones = Notificaciones::where('user_id',auth()->user()->id)->where('estado', 'No leido')->orderBy('created_at','DESC')->get();
        $user = auth()->user();

        $array['user'] = $user;
        $array['notificaciones'] = $notificaciones;

        if (request()->wantsJson()){
            return $array;
        }

    	return view('notificaciones',[
    		'user' => $user,
            'notificaciones' => $notificaciones,
    	]);
    }

    public function dismiss($id)
    {
        $notificacion = Notificaciones::find($id);
        $notificacion->estado = "Leido";
        $notificacion->save();
        
        $notificaciones = Notificaciones::where('user_id',auth()->user()->id)->where('estado', 'No leido')->orderBy('created_at','DESC')->get();
        $user = auth()->user();

        $array['user'] = $user;
        $array['notificaciones'] = $notificaciones;

        if (request()->wantsJson()){
            return $array;
        }

        return view('notificaciones',[
            'user' => $user,
            'notificaciones' => $notificaciones,
        ]);
    }

    public function notificaHora($request)
    {

        // Notificacion del Usuario
        $notificacion = New Notificaciones;
        $notificacion->user_id = auth()->user()->id;
        // $notificacion->terapeuta_id = $request->terapeuta;
        // $notificacion->perfil_notificacion = 1;
        $notificacion->tipo_notificacion = "Solicitud Hora";
        $notificacion->titulo = "Solicitud de Hora Realizada!";
        $notificacion->extracto = "La solicitud de hora de la terapia " . Terapias::find($request->terapia)->nombre . " fue realizada.";
        $notificacion->descripcion = "Se ha hecho una solicitud de hora al terapeuta " . User::find($request->terapeuta)->name . " para el día " . Horarios::find($request->hora)->fecha . " a las " . Horarios::find($request->hora)->hora . ". Se enviaron mas detalles a tu correo.";
        $notificacion->estado = "No leido";
        $notificacion->save();

        // Notificacion del Terapeuta
        $notificacion = New Notificaciones;
        $notificacion->user_id = $request->terapeuta;
        // $notificacion->terapeuta_id = auth()->user()->id;
        // $notificacion->perfil_notificacion = 2;
        $notificacion->tipo_notificacion = "Solicitud Hora";
        $notificacion->titulo = "Te han solicitado una Hora!";
        $notificacion->extracto = "El usuario " . auth()->user()->name . " te ha solicitado una hora para la terapia " . Terapias::find($request->terapia)->nombre . ".";
        $notificacion->descripcion = "Se ha solicitado una hora para el día " . Horarios::find($request->hora)->fecha . " a las " . Horarios::find($request->hora)->hora . ".";
        $notificacion->estado = "No leido";
        $notificacion->save();
    }

    public function notificaNuevaTerapia($request)
    {
        // Notificacion del Admin
        $notificacion = New Notificaciones;
        $notificacion->user_id = auth()->user()->id;
        // $notificacion->terapeuta_id = $request->terapeuta;
        // $notificacion->perfil_notificacion = 1;
        $notificacion->tipo_notificacion = "Nueva Terapia";
        $notificacion->titulo = "Nueva Terapia Registrada!";
        $notificacion->extracto = "Se realizó el registro de la nueva terapia '" . $request->nombre . "'.";
        $notificacion->descripcion = "Se ha registrado la nueva terapia '" . $request->nombre . "' con la siguiente descripción: '" . $request->descripcion . "'.";
        $notificacion->estado = "No leido";
        $notificacion->save();
    }

    public function notificaSolicitudTerapeuta($request)
    {
        // dd($request);

        $user = User::find($request['id_user']);
        $terapia = Terapias::find($request['terapia_id']);
        $admin = User::where('id_perfil',3)->get();

        // Notificaciones de los Admin - LLEGA UNA NOTIFICACION A CADA ADMINISTRADOR EN CASO DE HABER MAS DE UNO
        foreach ($admin as $key => $value) {
            $notificacion = New Notificaciones;
            $notificacion->user_id = $value->id;
            // $notificacion->terapeuta_id = $request->terapeuta;
            // $notificacion->perfil_notificacion = 1;
            $notificacion->tipo_notificacion = "Nuevo Terapeuta";
            $notificacion->titulo = "Un usuario ha solicitado ser Terapeuta!";
            $notificacion->extracto = "El usuario '" . $user->name . "' ha solicitado convertirse en terapeuta.";
            $notificacion->descripcion = "El usuario '" . $user->name . "' ha solicitado ser terapeuta e impartir la terapia '" . $terapia->nombre . "' con la siguiente descripción: '" . $request['descripcion'] . "'.";
            $notificacion->estado = "No leido";
            $notificacion->save();
        }

        // Notificacion del Usuario
        $notificacion = New Notificaciones;
        $notificacion->user_id = $request['id_user'];
        // $notificacion->terapeuta_id = $request->terapeuta;
        // $notificacion->perfil_notificacion = 1;
        $notificacion->tipo_notificacion = "Nuevo Terapeuta";
        $notificacion->titulo = "Solicitud para ser Terapeuta Realizada!";
        $notificacion->extracto = "Haz solicitado pertenecer al equipo de Terapeutas.'";
        $notificacion->descripcion = "Solicitaste de forma exitosa pertenecer al equipo de Terapeutas de Terapias Now con la terapia '" . $terapia->nombre . "' y con la siguiente descripción: '" . $request['descripcion'] . "'.";
        $notificacion->estado = "No leido";
        $notificacion->save();
    }

    public function notificaUpdatePerfil($request)
    {
        // Notificacion del Usuario
        $notificacion = New Notificaciones;
        $notificacion->user_id = $request;
        // $notificacion->terapeuta_id = $request->terapeuta;
        // $notificacion->perfil_notificacion = 1;
        $notificacion->tipo_notificacion = "Update Perfil";
        $notificacion->titulo = "Perfil Actualizado";
        $notificacion->extracto = "Se ha actualizado tu perfil.";
        $notificacion->descripcion = "Haz actualizado tu perfil de forma exitosa.";
        $notificacion->estado = "No leido";
        $notificacion->save();
    }

    public function notificaSolicitudTerapia($request)
    {
        $user = User::find(auth()->user()->id);
        $terapia = Terapias::find($request['terapia']);
        $admin = User::where('id_perfil',3)->get();
        // Notificacion del admin
        foreach ($admin as $key => $value) {
            $notificacion = New Notificaciones;
            $notificacion->user_id = $value->id;
            // $notificacion->terapeuta_id = $request->terapeuta;
            // $notificacion->perfil_notificacion = 1;
            $notificacion->tipo_notificacion = "Solicitud de Terapia";
            $notificacion->titulo = "Un usuario ha solicitado una terapia!";
            $notificacion->extracto = "El usuario '" . $user->name . "' ha solicitado impartir una terapia.";
            $notificacion->descripcion = "El usuario '" . $user->name . "' ha solicitado impartir la terapia '" . $terapia->nombre . "' al siguiente precio: $" . $request['precio'] . ".- por lo que requiere de su verificación y aprobación. Contacte al terapeuta mediante el correo " . $user->email . " para agendar una cita.";
            $notificacion->estado = "No leido";
            $notificacion->save();
        }

        // Notificacion del Usuario
        $notificacion = New Notificaciones;
        $notificacion->user_id = $user->id;
        // $notificacion->terapeuta_id = $request->terapeuta;
        // $notificacion->perfil_notificacion = 1;
        $notificacion->tipo_notificacion = "Solicitud de Terapia";
        $notificacion->titulo = "Solicitud de Terapia Realizada";
        $notificacion->extracto = "Haz realizado la solicitud de terapia.";
        $notificacion->descripcion = "La solicitud para impartir la terapia " . $terapia->nombre . " esta siendo atendida por los administradores, los cuales le contactaran para mayores detalles.";
        $notificacion->estado = "No leido";
        $notificacion->save();
    }

    public function notificaSugerirTerapia($request)
    {
        $user = User::find(auth()->user()->id);
        $admin = User::where('id_perfil',3)->get();
        // Notificacion del admin
        foreach ($admin as $key => $value) {
            $notificacion = New Notificaciones;
            $notificacion->user_id = $value->id;
            // $notificacion->terapeuta_id = $request->terapeuta;
            // $notificacion->perfil_notificacion = 1;
            $notificacion->tipo_notificacion = "Sugerir Terapia";
            $notificacion->titulo = "Un usuario ha sugerido y solicitado una terapia!";
            $notificacion->extracto = "El usuario '" . $user->name . "' ha sugerido y solicitado impartir una terapia.";
            $notificacion->descripcion = "El usuario '" . $user->name . "' ha sugerido y solicitado impartir la terapia '" . $request->nombre . "' con la siguiente descripción '" . $request->descripcion . "' al siguiente precio: $" . $request->precio . ".- por lo que requiere de su verificación y aprobación. Contacte al terapeuta mediante el correo " . $user->email . " para agendar una cita.";
            $notificacion->estado = "No leido";
            $notificacion->save();
        }

        // Notificacion del Usuario
        $notificacion = New Notificaciones;
        $notificacion->user_id = $user->id;
        // $notificacion->terapeuta_id = $request->terapeuta;
        // $notificacion->perfil_notificacion = 1;
        $notificacion->tipo_notificacion = "Sugerir Terapia";
        $notificacion->titulo = "Sugerencia de Terapia Realizada";
        $notificacion->extracto = "Haz realizado una sugerencia y solicitud de terapia.";
        $notificacion->descripcion = "La solicitud para impartir una nueva terapia llamada " . $request->nombre . " esta siendo atendida por los administradores, los cuales le contactaran para mayores detalles.";
        $notificacion->estado = "No leido";
        $notificacion->save();
    }

    public function notificaAprobacion($request)
    {
        $user = User::find(auth()->user()->id);
        
        // Notificacion del Usuario
        $notificacion = New Notificaciones;
        $notificacion->user_id = $user->id;
        $notificacion->tipo_notificacion = "Aprobacion";
        $notificacion->titulo = "Su solicitud de " . $request['tipo_solicitud'] . " fue aprobada.";
        $notificacion->extracto = "Un administrador a aprobado tu solicitud.";
        $notificacion->descripcion = $request['descripcion'];
        $notificacion->estado = "No leido";
        $notificacion->save();
    }
}
