<?php

namespace App\Http\Controllers;

use App\Noticias;
use Illuminate\Http\Request;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $array['user'] = $user;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.menu_noticias',[
            'user' => $user,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();

        $array['user'] = $user;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.noticias.crear_noticias',[
            'user' => $user,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->hasFile('imagen')){
            $filenameWithExt = $request->file('imagen')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $fileNameToStore = date('Y-m-d_H-i-s') . '_' . uniqid() . '_noticia' . '.' .$extension;
            request()->imagen->move(public_path('imagenes/noticias/'), $fileNameToStore);
        } else {
            $fileNameToStore = "default.png";
        }

        $noticia = new Noticias;
        $noticia->titulo = $request->titulo;
        $noticia->detalle = $request->detalle;
        $noticia->fecha = $request->fecha;
        $noticia->extracto = $request->extracto;
        $noticia->imagen = $fileNameToStore;

        $noticia->save();

        $user = auth()->user();
        $mensaje = 'Noticia registrada exitosamente';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.noticias',[
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $noticia = Noticias::find($id);
        $user = auth()->user();

        $array['user'] = $user;
        $array['noticia'] = $noticia;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.noticias.show',[
            'user' => $user,
            'noticia' => $noticia
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $noticia = Noticias::find($id);
        $user = auth()->user();

        $array['user'] = $user;
        $array['noticia'] = $noticia;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.noticias.edit',[
            'user' => $user,
            'noticia' => $noticia
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $noticia = Noticias::find($id);
        $noticia->titulo = $request->titulo;
        $noticia->extracto = $request->extracto;
        $noticia->detalle = $request->detalle;
        if ($request->hasFile('imagen')){
            $filenameWithExt = $request->file('imagen')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $fileNameToStore = date('Y-m-d_H-i-s') . '_' . uniqid() . '_noticia' . '.' .$extension;
            request()->imagen->move(public_path('imagenes/noticias/'), $fileNameToStore);
            $noticia->imagen = $fileNameToStore;
        }

        $noticia->save();

        $user = auth()->user();
        $mensaje = 'Noticia actualizada exitosamente.';

        $array['user'] = $user;
        $array['noticia'] = $noticia;
        $array['mensaje'] = $mensaje;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.noticias.show',[
            'user' => $user,
            'noticia' => $noticia,
            'mensaje' => $mensaje,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $noticia = Noticias::find($id);
        $noticia->delete();

        $user = auth()->user();
        $noticias = Noticias::all();
        $mensaje = 'Noticia eliminada exitosamente.';

        $array['user'] = $user;
        $array['noticias'] = $noticias;
        $array['mensaje'] = $mensaje;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.noticias.lista',[
            'user' => $user,
            'noticias' => $noticias,
            'mensaje' => $mensaje,
        ]);
    }

    public function listar()
    {
        $user = auth()->user();
        $noticias = Noticias::all();

        $array['user'] = $user;
        $array['noticias'] = $noticias;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.noticias.lista',[
            'user' => $user,
            'noticias' => $noticias
        ]);
    }
}
