<?php

namespace App\Http\Controllers;

use App\Palabras;
use Illuminate\Http\Request;

class PalabrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $palabras = Palabras::all();
        $user = auth()->user();

        $array['user'] = $user;
        $array['palabras'] = $palabras;

        if (request()->wantsJson()){
            return $array;
        }

        return view('perfiles.admin.palabras_clave.create_palabras',[
            'palabras' => $palabras,
            'user' => $user,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $palabra = new Palabras;
        $palabra->palabra = $request->palabra;
        $palabra->save();

        $palabras = Palabras::all();
        $user = auth()->user();
        $mensaje = 'Palabra ingresada exitosamente!';
    dd($mensaje);
        $array['user'] = $user;
        $array['palabras'] = $palabras;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson()){
            return $array;
        }

        return view('perfiles.admin.palabras_clave.create_palabras',[
            'palabras' => $palabras,
            'user' => auth()->user(),
            'mensaje' => "Palabra ingresada exitosamente!",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function elimina(Request $request)
    {
        $palabra = Palabras::find($request->palabra);
        $palabra->delete();

        $palabras = Palabras::all();
        $user = auth()->user();
        $mensaje = 'Palabra eliminada exitosamente!';

        $array['user'] = $user;
        $array['palabras'] = $palabras;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson()){
            return $array;
        }

        return view('perfiles.admin.palabras_clave.create_palabras',[
            'palabras' => $palabras,
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }
}
