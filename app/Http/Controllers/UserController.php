<?php

namespace App\Http\Controllers;

use App\CentrosTerapeuticos;
use App\Terapias;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $users = User::all();

        $array['user'] = $user;
        $array['users'] = $users;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapeutas.lista',[
            'user' => $user,
            'users' => $users,
        ]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = User::find($id);

        $user = auth()->user();

        $array['user'] = $user;
        $array['cliente'] = $cliente;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.userShow',[
            'cliente' => $cliente,
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $centros = CentrosTerapeuticos::all();

        $array['user'] = $user;
        $array['centros'] = $centros;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.user.update', [
            'user' => $user,
            'centros' => $centros,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($request->has('direccion')) {
            $user->direccion = $request->direccion;
        }
        if ($request->has('fecha_nacimiento')) {
            $user->fecha_nacimiento = $request->fecha_nacimiento;
        }
        if ($request->has('phone')) {
            $user->phone = $request->phone;
        }
        if ($request->has('email')) {
            $user->email = $request->email;
        }
        if ($request->hasFile('imagen')){
            $filenameWithExt = $request->file('imagen')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $fileNameToStore = date('Y-m-d_H-i-s') . '_' . uniqid() . '_usuarios' . '.' .$extension;
            request()->imagen->move(public_path('imagenes/usuarios/'), $fileNameToStore);
            $user->imagen = $fileNameToStore;
        }
        $user->save();

        app('App\Http\Controllers\NotificacionesController')->notificaUpdatePerfil($id);

        $user = User::find($id);
        $centros = CentrosTerapeuticos::all();
        $mensaje = 'Usuario Actualizado Exitosamente';

        $array['user'] = $user;
        $array['centros'] = $centros;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.user.update',[
            'user' => $user,
            'centros' => $centros,
            'mensaje' => $mensaje,
        ]);
    }

    public function formulario($id)
    {
        $user = User::find($id);
        $terapias = Terapias::all();

        $array['user'] = $user;
        $array['terapias'] = $terapias;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.user.solicita',[
            'user' => $user,
            'terapias' => $terapias,
        ]);
    }

    public function solicita(Request $request, $id)
    {
        $user = User::find($id);

        $solicitud['user_id'] = $id;
        $solicitud['terapia_id'] = $request->terapias;
        $solicitud['descripcion'] = $request->descripcion;
        $solicitud['precio'] = $request->precio;
        $solicitud['tipo_solicitud'] = "Solicitud de Terapeuta";

        // Solicitud al Administrador
        app('App\Http\Controllers\SolicitudesController')->store($solicitud);

        $notificacion['descripcion'] = $request->descripcion;
        $notificacion['terapia_id'] = $request->terapias;
        $notificacion['id_user'] = $id;

        // Notificacion al administrador
        app('App\Http\Controllers\NotificacionesController')->notificaSolicitudTerapeuta($notificacion);

        $mensaje = 'Solicitud realizada exitosamente, nos comunicaremos contigo en los proximos días!';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.index',[
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

    public function descripcion($id)
    {

        $user = User::find($id);

        $array['user'] = $user;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.user.descripcion',[
            'user' => $user,
        ]);
    }

    public function descripcionupdate(Request $request, $id)
    {
        $user = User::find($id);
        $user->descripcion = $request->descripcion;
        $user->save();

        $user = User::find($id);
        $mensaje = 'Descripción agregada exitosamente!';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.index',[
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

    public function verterapeuta($id)
    {
        $usuario = User::find($id);
        $user = auth()->user();

        $array['user'] = $user;
        $array['usuario'] = $usuario;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapeutas.show',[
            'usuario' => $usuario,
            'user' => $user,
        ]);
    }

    public function adminedit($id)
    {
        $terapeuta = User::find($id);
        $centros = CentrosTerapeuticos::all();
        $user = auth()->user();

        $array['terapeuta'] = $terapeuta;
        $array['user'] = $user;
        $array['centros'] = $centros;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapeutas.update',[
            'terapeuta' => $terapeuta,
            'user' => $user,
            'centros' => $centros,
        ]);
    }

    public function adminupdate(Request $request, $id)
    {
        $usuario = User::find($id);
        $usuario->id_perfil = $request->tipo;
        $usuario->centro_id = $request->centro;
        $usuario->save();

        $user = auth()->user();
        $mensaje = 'Terapeuta Actualizado Exitosamente!';

        $array['usuario'] = $usuario;
        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapeutas.show',[
            'usuario' => $usuario,
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

    public function admindestroy($id)
    {
        $usuario = User::find($id);
        $usuario->destroy();

        $user = auth()->user();
        $mensaje = 'Terapeuta Eliminado Exitosamente!';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.menu_terapeutas', [
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

}
