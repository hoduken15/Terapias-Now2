<?php

namespace App\Http\Controllers;

use App\Horarios;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ScheduleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = auth()->user();

        $array['user'] = $usuario;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('schedule.index',[
            'user' => $usuario,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        $h = 9;
        while ($h < 21) {
            $key = date('H:i', strtotime(date('Y-m-d') . ' + ' . $h . ' hours'));
            $value = date('H:i', strtotime(date('Y-m-d') . ' + ' . $h . ' hours'));
            $horas[$key] = $value;
            $h++;
        }

        $hoy = Carbon::now()->setTimezone('America/Santiago');
        // $hoy = Carbon::now();
        if ($hoy->isMonday()) {
            $hoy->addDays(7);
        } elseif ($hoy->isTuesday()) {
            $hoy->addDays(6);
        } elseif ($hoy->isWednesday()) {
            $hoy->addDays(5);
        } elseif ($hoy->isThursday()) {
            $hoy->addDays(4);
        } elseif ($hoy->isFriday()) {
            $hoy->addDays(3);
        } elseif ($hoy->isSaturday()) {
            $hoy->addDays(2);
        } elseif ($hoy->isSunday()) {
            $hoy->addDays(1);
        } else {
            $mensaje = 'Aún no puedes crear tu siguiente horario de trabajo.';

            $array['user'] = $user;
            $array['mensaje'] = $mensaje;

            if (request()->wantsJson() ) {
                return $array;
            }

            return view('schedule.create',[
                'user' => $user,
                'mensaje' => $mensaje,
            ]);
        }

        $ultimodia = Horarios::where('semana', $hoy->weekOfYear)->where('año', $hoy->weekYear)->where('user_id', $user->id)->get();
        if (!$ultimodia->isEmpty()) {
            $mensaje = "Ya haz creado el horario de trabajo de la siguiente semana, si quieres editarlo ve a la opción 'Editar'.";

            $array['user'] = $user;
            $array['mensaje'] = $mensaje;

            if (request()->wantsJson() ) {
                return $array;
            }

            return view('schedule.create',[
                'user' => $user,
                'mensaje' => $mensaje,
            ]);
        }

        for ($i=1; $i <= 7; $i++) { 
            $dia[$i] = $hoy->format('d-m-Y');
            $hoy->addDay();
        }
        
        $array['horas'] = $horas;
        $array['dia'] = $dia;
        $array['user'] = $user;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('schedule.create',[
            'horas' => $horas,
            'dia' => $dia,
            'user' => $user,
        ]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $recuest = $request->except(['_token']);
        $collection = collect($recuest);
        $hoy = Carbon::createFromFormat('Y-m-d', '2019-01-01', 'America/Santiago');
        foreach ($collection as $key => $value) {
            foreach ($value as $hora) {
                Horarios::create([
                    'user_id'       => $user->id,
                    'semana'        => $hoy->carbonize($key)->weekOfYear,
                    'año'           => $hoy->carbonize($key)->year,
                    'fecha'         => $hoy->carbonize($key),
                    'hora'          => $hora,
                    'estado'        => 'Disponible'
                ]);
            }
        }
        
        $mensaje = 'Calendario creado exitosamente';

        $array['user'] = $user;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('schedule.index',[
            'user' => $user,
            'mensaje' => $mensaje,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $horario = Horarios::where('user_id', $id)
            ->where('semana', Carbon::now()->setTimezone('America/Santiago')->weekOfYear)
            ->where('año', Carbon::now()->setTimezone('America/Santiago')->weekYear)
            ->get();

        $h = 9;
        while ($h < 21) {
            $key = date('H:i:s', strtotime(date('Y-m-d') . ' + ' . $h . ' hours'));
            $value = date('H:i:s', strtotime(date('Y-m-d') . ' + ' . $h . ' hours'));
            $horas[$key] = $value;
            $h++;
        }

        $hoy = Carbon::now()->setTimezone('America/Santiago')->startOfWeek();
        
        for ($i=1; $i <= 7; $i++) { 
            $dia[$i] = $hoy->format('d-m-Y');
            $hoy->addDay();
        }

        $var = [];
        foreach ($horario as $key => $value) {
            $x = date('d-m-Y', strtotime($value->fecha)) . "|" . $value->hora;
            $var[$x] = $value->id;
        }

        $var = collect($var);

        $horario = $horario->keyBy(function ($item) {
            return date('d-m-Y', strtotime($item['fecha'])) . '|' . $item['hora'];
        });

        $user = User::find($id);
        $mensaje = "Este terapeuta no tiene horario todavia";

        $array['horarios'] = $horario;
        $array['var'] = $var;
        $array['user'] = $user;
        $array['horas'] = $horas;
        $array['dias'] = $dia;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('schedule.show',[
            'horarios' => $horario,
            'var' => $var,
            'user' => $user,
            'horas' => $horas,
            'dias' => $dia,
            'mensaje' => $mensaje,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth()->user();

        $array['user'] = $user;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('schedule.update',[
            'user' => $user,
        ]);
    }

}
