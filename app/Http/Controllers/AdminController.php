<?php

namespace App\Http\Controllers;

use App\Palabras;
use App\PalabrasTerapias;
use App\Terapias;
use App\TerapiasUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function terapeutas()
    {
        $terapeutas = User::where('id_perfil',2)->get();
        $usuario = Auth::user();

        $array['usuario'] = $usuario;
        $array['terapeutas'] = $terapeutas;

        if ( request()->wantsJson() ) {
            return $array;
        }

    	return view('perfiles.admin.menu_terapeutas',[
    		'user' => $usuario,
    	]);
    }

    public function usuarios()
    {
        $usuario = User::where('id_perfil',1)->get();
        $user = Auth::user();

        $array['user'] = $user;
        $array['usuario'] = $usuario;

        if ( request()->wantsJson() ) {
            return $array;
        }

    	return view('perfiles.admin.usuarios',[
    		'usuarios' => $usuario,
    		'user' => $user,
    	]);	
    }

    public function terapias()
    {
        $terapias = Terapias::get();
        $user = Auth::user();

        $array['terapias'] = $terapias;
        $array['user'] = $user;

        if ( request()->wantsJson() ) {
            return $array;
        }

    	return view('perfiles.admin.menu_terapias',[
    		'terapias' => $terapias,
    		'user' => $user,
    	]);
    }

    public function showPalabrasTerapias()
    {
        $palabras = Palabras::all();
        $terapias = Terapias::where('estado', 'Aprobado')->get();

        $array['palabras'] = $palabras;
        $array['terapias'] = $terapias;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.palabras_clave.asignar',[
            'palabras' => $palabras,
            'terapias' => $terapias,
            'user' => auth()->user()
        ]);
    }

    public function PalabrasTerapias(Request $request)
    {
        $palabrasterapias = new PalabrasTerapias;
        $palabrasterapias->palabras_id = $request->palabra;
        $palabrasterapias->terapias_id = $request->terapia;
        $palabrasterapias->save();
        $terapias = Terapias::all();
        $palabras = Palabras::all();
        $usuarios = User::all();
        $user = auth()->user();

        $array['usuarios'] = $usuarios;
        $array['user'] = $user;
        $array['terapias'] = $terapias;
        $array['palabras'] = $palabras;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.palabras_clave.asignar',[
            'usuarios' => $usuarios,
            'user' => $user,
            'terapias' => $terapias,
            'palabras' => $palabras,
        ]);
    }

    public function verAgregaTerapia($id)
    {
        $terapia = Terapias::find($id);
        $user = auth()->user();
        $usuarios = User::all();

        $array['usuarios'] = $usuarios;
        $array['user'] = $user;
        $array['terapia'] = $terapia;

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.terapias.asigna',[
            'usuarios' => $usuarios,
            'user' => $user,
            'terapia' => $terapia,
        ]);
    }

    public function AgregaTerapia(Request $request, $id)
    {
        $terapiasuser = new TerapiasUser;
        $terapiasuser->terapias_id = $id;
        $terapiasuser->user_id = $request->terapeuta;
        $terapiasuser->precio = $request->precio;
        $terapiasuser->save();
        $usuarios = User::all();
        $user = auth()->user();

        $array['usuarios'] = $usuarios;
        $array['user'] = $user;
        $array['mensaje'] = 'Terapias Asignada Exitosamente!';

        if ( request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.menu_terapias',[
            'usuarios' => $usuarios,
            'user' => $user,
            'mensaje' => 'Terapias Asignada Exitosamente!'
        ]);
    }

}
