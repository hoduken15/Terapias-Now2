<?php

namespace App\Http\Controllers;

use App\Solicitudes;
use App\Terapias;
use App\TerapiasUser;
use App\User;
use Illuminate\Http\Request;

class SolicitudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $solicitudes = Solicitudes::where('estado','pendiente')->get();

        $array['user'] = $user;
        $array['solicitudes'] = $solicitudes->load(['Terapias','Usuarios']);

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.solicitudes.lista',[
            'user' => $user,
            'solicitudes' => $solicitudes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $solicitud = New Solicitudes;
        $solicitud->user_id = $request['user_id'];
        $solicitud->terapia_id = $request['terapia_id'];
        $solicitud->descripcion = $request['descripcion'];
        $solicitud->precio = $request['precio'];
        $solicitud->estado = 'pendiente';
        $solicitud->tipo_solicitud = $request['tipo_solicitud'];
        $solicitud->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $solicitud = Solicitudes::find($id);

        $array['user'] = $user;
        $array['solicitud'] = $solicitud;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.solicitudes.show',[
            'user' => $user,
            'solicitud' => $solicitud,
        ]);
    }

    public function aprobado($id)
    {
        $solicitud = Solicitudes::find($id);
        if ($solicitud->tipo_solicitud == "Solicitud de Terapeuta") {
            $user = User::find($solicitud->user_id);
            $user->id_perfil = 2;
            $user->save();
        } elseif ($solicitud->tipo_solicitud == "Solicitud de Terapia") {
            $terapiaterapeuta = new TerapiasUser;
            $terapiaterapeuta->terapias_id = $solicitud->terapia_id;
            $terapiaterapeuta->user_id = $solicitud->user_id;
            $terapiaterapeuta->precio = $solicitud->precio;
            $terapiaterapeuta->save();
        } elseif ($solicitud->tipo_solicitud == "Sugerencia de Terapia") {
            $terapia = Terapias::find($solicitud->terapia_id);
            $terapia->estado = "Aprobado";
            $terapia->save();
            $terapiaterapeuta = new TerapiasUser;
            $terapiaterapeuta->terapias_id = $solicitud->terapia_id;
            $terapiaterapeuta->user_id = $solicitud->user_id;
            $terapiaterapeuta->precio = $solicitud->precio;
            $terapiaterapeuta->save();
        }
        $solicitud->estado = "aprobado";
        $solicitud->save();
        $request['tipo_solicitud'] = $solicitud->tipo_solicitud;
        $request['descripcion'] = $solicitud->descripcion;
        app('App\Http\Controllers\NotificacionesController')->notificaAprobacion($request);

        $user = auth()->user();
        $solicitudes = Solicitudes::where('estado','pendiente')->get();
        $mensaje = 'Solicitud aprobada exitosamente.';

        $array['user'] = $user;
        $array['solicitudes'] = $solicitudes;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.solicitudes.lista',[
            'user' => $user,
            'solicitudes' => $solicitudes,
            'mensaje' => $mensaje,
        ]);
    }

    public function rechazado($id)
    {
        $solicitud = Solicitudes::find($id);
        $solicitud->estado = "rechazado";
        $solicitud->save();
        $solicitudes = Solicitudes::where('estado','pendiente')->get();
        $user = auth()->user();

        $mensaje = 'Solicitud rechazada exitosamente.';

        $array['user'] = $user;
        $array['solicitudes'] = $solicitudes;
        $array['mensaje'] = $mensaje;

        if (request()->wantsJson() ) {
            return $array;
        }

        return view('perfiles.admin.solicitudes.lista',[
            'user' => $user,
            'solicitudes' => $solicitudes,
            'mensaje' => $mensaje,
        ]);
    }
}
