<?php

namespace App\Http\Controllers;

use App\Comentarios;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PerfilController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usuario = auth()->user();
    	$puntaje = Comentarios::where('id_cliente',$usuario->id)->get();

        if ($usuario->fecha_nacimiento != null) {
            $usuario->fecha_nacimiento = Carbon::parse($usuario->fecha_nacimiento)->age;
        } else {
            $usuario->fecha_nacimiento = 0;
        }
        if ($usuario->centro_id != null) {
            $usuario->centro_id = $usuario->Centros->nombre;
        } else {
            $usuario->centro_id = 0;
        }

        $array['user'] = $usuario;
        $array['puntaje'] = $puntaje;

        if (request()->wantsJson() ) {
            return $array;
        }

    	return view('perfiles.index',[
            'user' => $usuario,
            'puntaje' => $puntaje,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$puntos = Comentarios::where('id_terapeuta',$id)->get();

    	$puntaje = $puntos->pluck('puntaje')->avg();

        $user = User::find($id);

        $array['user'] = $user;
        $array['puntaje'] = $puntaje;

        if (request()->wantsJson()) {
            return $array;
        }

    	return view('perfiles.index',[
            'user' => $user,
            'puntaje' => $puntaje,
        ]);
    }

}
