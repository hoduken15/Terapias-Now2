<?php

namespace app\Services;

use App\Atenciones;
use App\Comentarios;
use App\Horarios;
use App\Terapias;
use App\User;

class ServicioDatos
{
	public function datos($id)
	{
		$datos = [];
		$comentarios = Comentarios::where('id_terapeuta', $id)->get();
		$datos['comentarios'] = $comentarios;
		$datos['puntaje'] = $comentarios->avg('puntaje');
		$datos['user'] = User::find($id)->load('Terapias');

		return $datos;

	}

	public function atenciones($id)
	{
		$datos = Atenciones::where('id_cliente', $id)->get();
		$atenciones = [];
		foreach ($datos as $key => $value) {
			$datos[$key]['user'] = User::find($value->id_cliente);
			$datos[$key]['terapeuta'] = User::find($value->id_terapeuta);
			$datos[$key]['terapia'] = Terapias::find($value->id_terapia);
			$datos[$key]['horario'] = Horarios::find($value->id_horario);
		}

		$atenciones = collect($datos);

		return $atenciones;
	}

	public function comentarios($id)
	{
		$comentarios = Comentarios::where('id_cliente', $id)->get();
		$datos = [];
		foreach($comentarios as $key => $value){
			$datos[$key]['cliente'] = User::find($value->id_cliente);
			$datos[$key]['terapeuta'] = User::find($value->id_terapeuta);
			$datos[$key]['puntaje'] = $value->puntaje;
			$datos[$key]['comentario'] = $value->comentario;
		}

		$evaluaciones = collect($datos);

		return $evaluaciones;
	}

}
