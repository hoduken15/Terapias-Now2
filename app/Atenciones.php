<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atenciones extends Model
{
	public function Cliente()
    {
        return $this->belongsTo('App\User','id_cliente','id');
    }

	public function Terapeuta()
    {
        return $this->belongsTo('App\User','id_terapeuta','id');
    }

    public function Terapia()
    {
        return $this->belongsTo('App\Terapias','id_terapia','id');
    }

    public function Horario()
    {
        return $this->belongsTo('App\Horarios','id_horario','id');
    }
}
