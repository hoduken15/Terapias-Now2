<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PalabrasTerapias extends Model
{
    protected $table = 'palabras_terapias';
}
