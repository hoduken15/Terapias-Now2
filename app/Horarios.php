<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horarios extends Model
{
    
	protected $fillable = [
        'user_id', 'semana', 'año', 'fecha', 'hora', 'estado',
    ];

	public function Horarios()
    {
    	return $this->belongsToMany('App\User');
    }

}
